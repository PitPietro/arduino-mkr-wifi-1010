#include <Arduino_LSM6DSOX.h>  // https://www.arduino.cc/reference/en/libraries/arduino_lsm6dsox/

// LSM6DSOX has two functions for temperature reading
int int_temperature;
float float_temperature;

/**
   Access Temperature Data on Nano RP2040 Connect

   Operating temperature range: -40 to +85 °C

   Docs: https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-imu-basics
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial)
    ;

  // initialize the IMU, return 1 on success and 0 on failure
  // de-initialize the IMU with 'IMU.end()' void function
  if (!IMU.begin()) {
    Serial.println("### Failed to initialize IMU ###");
    while (1)
      ;
  }

  Serial.println("Temperature: int & float");
}

void loop() {
  // query if new temperature data from the IMU is available
  if (IMU.temperatureAvailable()) {

    // query the IMU's temperature in Celsius
    IMU.readTemperature(int_temperature);         // int implementation
    IMU.readTemperatureFloat(float_temperature);  // float implementation

    Serial.print("int:");
    Serial.print(int_temperature);
    Serial.print("\t");
    Serial.print("float:");
    Serial.print(float_temperature);
    Serial.print("\n");
  }

  // add a short delay on the bottom of loop()
  delay(500);
}
