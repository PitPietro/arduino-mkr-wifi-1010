#include <PDM.h> // https://docs.arduino.cc/learn/built-in-libraries/pdm

// default number of output channels
static const char channels = 1;

// default PCM output frequency
static const int frequency = 16000;

// buffer to read samples into, each sample is 16-bits
short sample_buffer[512];

// number of audio samples read
volatile int samples_read;

/**
   Access Microphone Data on Nano RP2040 Connect

   MP34DT05 sensor is a ultra-compact microphone that use PDM (Pulse-Density Modulation) to represent an analog signal with a binary signal.
   The sensor's range of different values are the following:
   - Signal-to-noise ratio: 64dB
   - Sensitivity: -26dBFS ±3dB
   - Temperature range: -40 to 85°C

   Reads audio data from the on-board MP34DT05 microphone and prints out the samples to the Serial console.
   Open the Serial Plotter (Tools -> Serial Plotter, or Ctrl + Shift + L) to visually see the data.

   Docs: https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-microphone-basics
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  // set the callback function called when new PDM data is ready to be read
  PDM.onReceive(onPDMdata);

  // optionally set the gain with values frm 0 to 255, defaults to 20 if not specified
  // PDM.setGain(20);

  // optionally set the buffer size used by the PDM interface (set before PDM.begin(...) call)
  // a default buffer size of 512 bytes is used if not called
  // PDM.setBufferSize(512)

  // initialize PDM with:
  // - one channel (mono mode)
  // - a 16 kHz sample rate for the Arduino Nano 33 BLE Sense
  // - a 32 kHz or 64 kHz sample rate for the Arduino Portenta Vision Shields
  if (!PDM.begin(channels, frequency)) {
    Serial.println("Failed to start PDM!");
    while (1);
  }

  // de-initialize PDM mcrophone with 'PDM.end()' void function
}

void loop() {
  // wait for samples to be read
  if (samples_read) {

    // print samples to the serial monitor or plotter
    for (int i = 0; i < samples_read; i++) {
      if (channels == 2) {
        Serial.print("L:");
        Serial.print(sample_buffer[i]);
        Serial.print(" R:");
        i++;
      }
      Serial.println(sample_buffer[i]);
    }

    // clear the read count
    samples_read = 0;
  }

  delay(50);
}

/**
   Callback function to process the data from the PDM microphone
   PLEASE NOTE: This callback is executed as part of an ISR (Interrupt Service Routine)
   Using 'Serial' object to print messages inside this function isn't supported
*/
void onPDMdata() {
  // get the number of bytes available for reading from the PDM interface
  // data has already arrived and was stored in the PDM receive buffer
  // returns the number of bytes available to read
  int bytes_available = PDM.available();

  // read data from the PDM into the specified buffer
  PDM.read(sample_buffer, bytes_available);

  // 16-bit, 2 bytes per sample
  samples_read = bytes_available / 2;
}
