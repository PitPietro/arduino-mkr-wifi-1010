#include <WiFiNINA.h> // https://www.arduino.cc/reference/en/libraries/wifinina/

/**
   Arduino Nano RP2040 Connect - Built-in RGB Test

   While using Bluetooth® Low Energy mode on the NINA module, the RGB cannot be used by default.
   While the module is in Bluetooth® Low Energy mode, SPI is deactivated, which is used to control the RGBs.

   Docs: https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-01-technical-reference#rgb
*/
void setup() {
  pinMode(LEDR, OUTPUT);
  pinMode(LEDG, OUTPUT);
  pinMode(LEDB, OUTPUT);
}

void loop() {
  /// LEDs can be written with LOW, HIGH or a value between 0 and 255

  // low
  digitalWrite(LEDR, LOW);
  digitalWrite(LEDG, LOW);
  digitalWrite(LEDB, LOW);
  delay(1000);

  // high
  digitalWrite(LEDR, HIGH);
  digitalWrite(LEDG, HIGH);
  digitalWrite(LEDB, HIGH);
  delay(1000);

  // red
  analogWrite(LEDR, 255);
  analogWrite(LEDG, 0);
  analogWrite(LEDB, 0);
  delay(1000);

  // green
  analogWrite(LEDR, 0);
  analogWrite(LEDG, 255);
  analogWrite(LEDB, 0);
  delay(1000);

  // blue
  analogWrite(LEDR, 0);
  analogWrite(LEDG, 0);
  analogWrite(LEDB, 255);
  delay(1000);

}
