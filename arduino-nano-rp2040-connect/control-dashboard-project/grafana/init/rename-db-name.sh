#!/usr/bin/env sh
OLD="DB_TABLE_NAME"
NEW="$1"

# --- for a single .json file ---
f="/arduino-dashboard.json"
BPATH="/tmp/"

# if the backup directory does not exist, create it
[ ! -d $BPATH ] && mkdir -p $BPATH || :

if [ -f $f -a -r $f ]; then
  /bin/cp -f $f $BPATH
  sed -i "s/$OLD/$NEW/g" "$f"
else
  echo "Error: Cannot read $f"
  exit 1;
fi

exit 0;
# code below will not be reached

# --- for all .json files in DPATH ---
#DPATH="$PWD/*.json"
#BPATH="$PWD/backup"
# if the backup directory does not exist, create it
#[ ! -d $BPATH ] && mkdir -p $BPATH || :
#for f in $DPATH
#do
#  if [ -f $f -a -r $f ]; then
#    /bin/cp -f $f $BPATH
#    sed -i "s/$OLD/$NEW/g" "$f"
#   else
#    echo "Error: Cannot read $f"
#  fi
#done