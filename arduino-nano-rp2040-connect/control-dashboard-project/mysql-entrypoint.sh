#!/usr/bin/env sh

# environment variables are defined in compose.yaml
# using environment list

echo "$HOME --- `whoami`"
echo "`ls -lha`"
echo "pwd --> `pwd`"

# generate database with the name of the specified environment variable
echo "CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE};" >> schema.sql
echo "USE ${MYSQL_DATABASE};" >> schema.sql

# concatenate the rest of the database definition
cat ./db_tables.sql >> schema.sql
rm ./db_tables.sql

# copy the schema into default directory docker-entrypoint-initdb.d
# cp schema.sql /docker-entrypoint-initdb.d/schema.sql
mv schema.sql /docker-entrypoint-initdb.d/

# NOT generate .cnf file to customize database port
# append the following configuration to the bottom of /etc/my.cnf
# echo "[mysqld]" >> /etc/my.cnf
# echo "port = ${PORT_IN}" >> /etc/my.cnf
# echo "bind-address = 127.0.0.1" >> /etc/my.cnf
# echo "mysqlx-bind-address = 127.0.0.1" >> /etc/my.cnf
#mv config.cnf /etc/mysql/conf.d/

#echo `cat  /docker-entrypoint-initdb.d/schema.sql`;
#cd /docker-entrypoint-initdb.d/

# restart mysqld server to load port change configuration
# /usr/sbin/mysqld restart


# continue with the default entrypoint behavior
# exec "$@"
# exec $@
/entrypoint.sh "$@"