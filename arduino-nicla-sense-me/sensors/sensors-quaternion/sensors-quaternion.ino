#include "Arduino_BHY2.h"

SensorQuaternion rotationVector(SENSOR_ID_RV);
SensorQuaternion rotationVectorWakeUp(SENSOR_ID_RV_WU);
SensorQuaternion gameRotationVector(SENSOR_ID_GAMERV);
SensorQuaternion gameRotationVectorWakeUp(SENSOR_ID_GAMERV_WU);
SensorQuaternion geomagneticRotationVector(SENSOR_ID_GEORV);
SensorQuaternion geomagneticRotationVectorWakeUp(SENSOR_ID_GEORV_WU);


/**
   Nicla Sense ME Quaternion Sensors

   Docs: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();

  rotationVector.begin();
  rotationVectorWakeUp.begin();
  gameRotationVector.begin();
  gameRotationVectorWakeUp.begin();
  geomagneticRotationVector.begin();
  geomagneticRotationVectorWakeUp.begin();

  Serial.println("\n### Quaternion Sensors\n");
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 1000) {
    lastCheck = millis();

    serialPrintSensorQuaternion("rotationVector", rotationVector);
    serialPrintSensorQuaternion("rotationVectorWakeUp", rotationVectorWakeUp);
    serialPrintSensorQuaternion("gameRotationVector", gameRotationVector);
    serialPrintSensorQuaternion("gameRotationVectorWakeUp", gameRotationVectorWakeUp);
    serialPrintSensorQuaternion("geomagneticRotationVector", geomagneticRotationVector);
    serialPrintSensorQuaternion("geomagneticRotationVectorWakeUp", geomagneticRotationVectorWakeUp);

    Serial.println("___________________________________________________________\n");
  }
}

void serialPrintSensorQuaternion(String sensor_name, SensorQuaternion sensor) {
  Serial.println(sensor_name + ": (" + sensor.x() + ", " + sensor.y() + ", " + sensor.z() + ", " + sensor.w() + ")");
}
