#include "Arduino_BHY2.h"

Sensor wakeGesture(SENSOR_ID_WAKE_GESTURE);
Sensor glanceGesture(SENSOR_ID_GLANCE_GESTURE);
Sensor pickupGesture(SENSOR_ID_PICKUP_GESTURE);
Sensor wristTiltGesture(SENSOR_ID_WRIST_TILT_GESTURE);

Sensor deviceOrientationWakeUp(SENSOR_ID_DEVICE_ORI_WU);
Sensor stationaryDetect(SENSOR_ID_STATIONARY_DET);
Sensor motionDetect(SENSOR_ID_MOTION_DET);
Sensor stepDetectorWakeUp(SENSOR_ID_STD_WU);


/**
   Nicla Sense ME Gestures Sensors

   All sensors of class Sensor cannot stay in the same sketch because of
   lack of memory: they're splitted into different sketches.

   This class include all Nicla sensors which are not from the other sensors's classes:
   - SensorOrientation
   - SensorXYZ
   - SensorQuaternion
   - SensorActivity
   - SensorBSEC

   Website: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();

  wakeGesture.begin();
  glanceGesture.begin();
  pickupGesture.begin();
  wristTiltGesture.begin();

  deviceOrientationWakeUp.begin();
  stationaryDetect.begin();
  motionDetect.begin();
  stepDetectorWakeUp.begin();
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 100) {
    lastCheck = millis();

    printSensor("wake_gesture", wakeGesture.value());
    printSensor("glance_gesture", glanceGesture.value());
    printSensor("pickup_gesture", pickupGesture.value());
    printSensor("wrist_tilt_gesture", wristTiltGesture.value());

    printSensor("device_orientation_wakeup", deviceOrientationWakeUp.value());
    printSensor("stationary_detect", stationaryDetect.value());
    printSensor("motion_detect", motionDetect.value());
    printSensor("step_detector_wakeup", stepDetectorWakeUp.value());

    Serial.print("\n");
  }
}

void printSensor(String sensor_name, float sensor_value) {
  Serial.print(sensor_name + ":");
  Serial.print(sensor_value);
  Serial.print("\t");
}
