#include "Arduino_BHY2.h"

// acc stands for accelerometer
SensorXYZ accPassthrough(SENSOR_ID_ACC_PASS);
SensorXYZ accUncalibrated(SENSOR_ID_ACC_RAW);
SensorXYZ accCorrected(SENSOR_ID_ACC);
SensorXYZ accOffset(SENSOR_ID_ACC_BIAS);
SensorXYZ accCorrectedWakeUp(SENSOR_ID_ACC_WU);
SensorXYZ accUncalibratedWakeUp(SENSOR_ID_ACC_RAW_WU);
SensorXYZ accOffsetWakeUp(SENSOR_ID_ACC_BIAS_WU);

SensorXYZ linearAcceleration(SENSOR_ID_LACC);
SensorXYZ linearAccelerationWakeUp(SENSOR_ID_LACC_WU);

SensorXYZ gravityVector(SENSOR_ID_GRA);
SensorXYZ gravityVectorWakeUp(SENSOR_ID_GRA_WU);



/**
   Nicla Sense ME Acceleromter & Linear Acceleration & Gravity Vector Sensors

   Docs: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();

  accPassthrough.begin();
  accUncalibrated.begin();
  accCorrected.begin();
  accOffset.begin();
  accCorrectedWakeUp.begin();
  accUncalibratedWakeUp.begin();
  accOffsetWakeUp.begin();

  linearAcceleration.begin();
  linearAccelerationWakeUp.begin();

  gravityVector.begin();
  gravityVectorWakeUp.begin();

  Serial.println("\n### Accelerometer & Linear Acceleration & Gravity Vector Sensors\n");
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 1000) {
    lastCheck = millis();

    Serial.println(String("Passthrough: (") + accPassthrough.x() + ", " + accPassthrough.y() + ", " + accPassthrough.z() + ")");
    Serial.println(String("Uncalibrated: (") + accUncalibrated.x() + ", " + accUncalibrated.y() + ", " + accUncalibrated.z() + ")");
    Serial.println(String("Corrected: (") + accCorrected.x() + ", " + accCorrected.y() + ", " + accCorrected.z() + ")");
    Serial.println(String("Offset: (") + accOffset.x() + ", " + accOffset.y() + ", " + accOffset.z() + ")");
    Serial.println(String("CorrectedWakeUp: (") + accCorrectedWakeUp.x() + ", " + accCorrectedWakeUp.y() + ", " + accCorrectedWakeUp.z() + ")");
    Serial.println(String("UncalibratedWakeUp: (") + accUncalibratedWakeUp.x() + ", " + accUncalibratedWakeUp.y() + ", " + accUncalibratedWakeUp.z() + ")");
    Serial.println(String("OffsetWakeUp: (") + accOffsetWakeUp.x() + ", " + accOffsetWakeUp.y() + ", " + accOffsetWakeUp.z() + ")");
    Serial.println("_______________________________________________________\n");
    Serial.println(String("linearAcceleration: (") + linearAcceleration.x() + ", " + linearAcceleration.y() + ", " + linearAcceleration.z() + ")");
    Serial.println(String("linearAccelerationWakeUp: (") + linearAccelerationWakeUp.x() + ", " + linearAccelerationWakeUp.y() + ", " + linearAccelerationWakeUp.z() + ")");
    Serial.println("_______________________________________________________\n");
    Serial.println(String("gravityVector: (") + gravityVector.x() + ", " + gravityVector.y() + ", " + gravityVector.z() + ")");
    Serial.println(String("gravityVectorWakeUp: (") + gravityVectorWakeUp.x() + ", " + gravityVectorWakeUp.y() + ", " + gravityVectorWakeUp.z() + ")");
    Serial.println("#######################################################\n");
  }
}
