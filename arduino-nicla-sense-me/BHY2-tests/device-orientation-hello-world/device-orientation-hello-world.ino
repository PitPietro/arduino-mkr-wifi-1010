#include "Arduino_BHY2.h"

SensorOrientation orientation(SENSOR_ID_ORI);

// Docs: https://docs.arduino.cc/tutorials/nicla-sense-me/cheat-sheet
void setup() {
  Serial.begin(115200);
  BHY2.begin();
  orientation.begin();
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  // check sensor values every 'delayTime' milliseconds
  if (millis() - lastCheck >= 1000) {
    lastCheck = millis();

    Serial.print("X-axis rotation = pitch: ");
    Serial.print(orientation.pitch());
    Serial.print("\tY-axis rotation = roll: ");
    Serial.print(orientation.roll());
    Serial.print("\tZ-axis rotation = heading: ");
    Serial.println(orientation.heading());
  }
}
