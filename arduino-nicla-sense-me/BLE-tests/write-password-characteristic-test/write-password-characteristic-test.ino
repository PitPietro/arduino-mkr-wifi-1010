/*
   Bluetooth Low Energy - Write Password Characteristic

   Please Note: ArduinoBLE.h library is required.
   https://github.com/arduino-libraries/ArduinoBLE

   Docs:
   1. https://www.arduino.cc/reference/en/libraries/arduinoble/ (Arduino reference)
   2. https://axodyne.com/2020/08/ble-uuids/ (UUIDs clarifier)

   GATT (General Attribute Profile)
   1. services & characteristics https://www.bluetooth.com/specifications/assigned-numbers/
   2. https://btprodspecificationrefs.blob.core.windows.net/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf
*/

#include "Nicla_System.h"
#include <ArduinoBLE.h>
#include "Arduino_BHY2.h"

#define BLE_SENSE_UUID(val) ("ffaabbcc-" val "-00ed-aabe-ff45bdff0123")

SensorBSEC bsec(SENSOR_ID_BSEC);

// from "16-bit UUID Numbers Document" document, the service below has:
// - Allocation Type: GATT Service
// - Allocated UUID: 0x180F
// - Device Information: Battery

// create a new Bluetooth® Low Energy service
// parameter: uuid (16-bit or 128-bit UUID in String format)
// https://www.arduino.cc/reference/en/libraries/arduinoble/bleservice/
BLEService bsecService(BLE_SENSE_UUID("0000"));

// https://www.arduino.cc/reference/en/libraries/arduinoble/blecharacteristic/

// IAQ value for regular use case
BLEUnsignedIntCharacteristic iaqChar(BLE_SENSE_UUID("1001"), BLERead | BLENotify);

// password test
BLEByteCharacteristic passwordChar("2A1C", BLERead | BLEWrite);


/**
   BSEC output values - iaq: 0   iaq_s: 0   b_voc_eq: 0.00   co2_eq: 0   accuracy: 0   comp_t: 0.00   comp_h: 0.00   comp_g: 0
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  // while (!Serial);

  BHY2.begin();
  bsec.begin();
  // initialize ArduinoBLE library
  if (!BLE.begin()) {
    Serial.println("Bluetooth® Low Energy failed to start!");
    while (1);
  }

  nicla::begin();
  // custom value in mA
  nicla::enableCharge(210);

  Serial.print("Device MAC Address: ");
  Serial.println(BLE.address());

  BLE.address().toUpperCase();

  // String to calculate the local and device name
  // Append last two hex values of MAC address to ble_name
  String ble_name = "Arduino Nicla Sense ME";
  ble_name += BLE.address()[BLE.address().length() - 5];
  ble_name += BLE.address()[BLE.address().length() - 4];
  ble_name += BLE.address()[BLE.address().length() - 2];
  ble_name += BLE.address()[BLE.address().length() - 1];

  Serial.print("Device name: ");
  Serial.println(ble_name);

  // set the name that will appear when scanning for Bluetooth® devices
  BLE.setLocalName(ble_name.c_str());
  BLE.setDeviceName(ble_name.c_str());

  // set the advertised service UUID used when advertising to the value of the BLEService provided
  BLE.setAdvertisedService(bsecService);

  // add the characteristics to the BLEService
  bsecService.addCharacteristic(iaqChar);
  bsecService.addCharacteristic(passwordChar);

  Serial.print("standard UUID: ");
  Serial.println(passwordChar.uuid());

  Serial.print("custom UUID: ");
  Serial.println(iaqChar.uuid());


  // add a BLEService to the set of services the BLE device provides
  BLE.addService(bsecService);

  // start advertising the BLE device
  BLE.advertise();
  Serial.println("Bluetooth® device active, waiting for connections...");
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  // Serial.println(String("BSEC info: ") + bsec.toString());

  // query the central BLE device connected: wait for a BLE central
  BLEDevice central_device = BLE.central();

  // if a central is connected to the peripheral
  if (central_device) {
    Serial.print("Connected to MAC address: ");
    Serial.println(central_device.address());

    // while the central is connected
    while (central_device.connected()) {
      BHY2.update();

      // check characteristic status every 2000ms
      if (millis() - lastCheck >= 2000) {
        lastCheck = millis();

        Serial.println(String("BSEC info: ") + bsec.toString());
        iaqChar.writeValue(bsec.iaq());

        // query if the characteristic value has been written
        if (passwordChar.written()) {
          Serial.print("!\n!\n!\npassword has been written: ");
          Serial.print(passwordChar.value());
          Serial.println("!\n!\n!\n");
        }
      }
    }

    Serial.println("Disconnected from central.");
  } else {
    // try to recconnect to central

    // query the central BLE device connected: wait for a BLE central
    central_device = BLE.central();
  }
}
