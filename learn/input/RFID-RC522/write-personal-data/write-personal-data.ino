/*
   Write personal data of a MIFARE RFID card using a RFID-RC522 reader

   Example sketch showing how to write personal data to a PICC (an RFID Tag or Card) using a MFRC522 based RFID Reader on the Arduino SPI interface.

   Open the Serial Monitor with Ctrl+Shft+M shortcut or by clicking Tools menu, then Serial Monitor.
   Follow the instructions given on the serial monitor

   Typical pin layout used:
   -----------------------------------------------------------------------------------------
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN 10
#define RST_PIN 9

// single block dimension
const byte SINGLE_B = 16;

// two blocks dimension
const byte DOUBLE_B = 32;

// instance of MFRC522 class
MFRC522 rfid(SS_PIN, RST_PIN);
MFRC522::MIFARE_Key key;

typedef struct {
  byte last_name[DOUBLE_B];  // 2 blocks
  byte first_name[DOUBLE_B]; // 2 blocks
  byte birth_date[SINGLE_B]; // 1 block: dd/mm/yyyy date format
} personal_data;

typedef struct {
  String last_name = "Type Family name and press Enter";
  String first_name = "Type First name and press Enter";
  String birth_date = "Type birth date in dd/mm/yyyy format and press Enter";
} personal_questions;

void buffer_serial_debugging(int num, byte * data_buffer) {
  Serial.print("#DEBUG# ");
  Serial.print(num);
  Serial.print(" ) ");
  Serial.print("buffer = @");
  Serial.print((char *) data_buffer);
  Serial.println("@\n");
}

/*
   Write two consequent blocks of data
*/
void write_two_block_data(int start_block, byte * data_buffer, byte buffer_length) {
  // pad with spaces
  pad_until_dimension(data_buffer, buffer_length, DOUBLE_B, ' ');

  // for (byte i = buffer_length; i <= DOUBLE_B; i++) data_buffer[i] = ' '

  for (int i = 0; i < 2; i++) {
    write_one_block_data(start_block + i, data_buffer, SINGLE_B * i);
  }
}

/*
   Write a single block of data
*/
void write_one_block_data(byte block, byte * data_buffer, int buffer_position) {
  MFRC522::StatusCode status;

  //Serial.println(F("Authenticating using key A..."));
  status = rfid.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(rfid.uid));

  // if the authentication fails, print the status code name and restart loop() function
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(rfid.GetStatusCodeName(status));
    return; // restart loop()
  } // else Serial.println(F("PCD_Authenticate() success"));


  // write block
  // pass buffer variable from given index since only 16 bytes can be written on a block
  status = rfid.MIFARE_Write(block, &data_buffer[buffer_position], SINGLE_B);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Write() failed: "));
    Serial.println(rfid.GetStatusCodeName(status));
    return; // restart loop()
  } // else Serial.println(F("MIFARE_Write() success"));
}

/*
   Pad data_buffer with pad_char character from buffer_length to buffer_dimension
*/
void pad_until_dimension(byte * data_buffer, byte buffer_length, byte buffer_dimension, byte pad_char) {
  for (byte i = buffer_length; i <= buffer_dimension; i++) {
    data_buffer[i] = pad_char;
  }
}


void setup() {
  Serial.begin(9600); // init Serial communication
  SPI.begin();        // init SPI bus
  rfid.PCD_Init();    // init MFRC522

  Serial.println("Write personal data on a MIFARE PICC");
}

void loop() {
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;

  // reset the loop if no new card present on the sensor/reader
  // this saves the entire process when IDLE
  if ( ! rfid.PICC_IsNewCardPresent())
    return; // restart loop()

  // verify if the NUID has been readed
  if ( ! rfid.PICC_ReadCardSerial())
    return; // restart loop()

  // dump UID
  Serial.print(F("Card UID:"));

  for (byte i = 0; i < rfid.uid.size; i++) {
    Serial.print(rfid.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(rfid.uid.uidByte[i], HEX);
  }

  // dump PICC type
  Serial.print(F(" PICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  Serial.println(rfid.PICC_GetTypeName(piccType));

  personal_data user_data;
  personal_questions user_questions;
  // byte buffer[DOUBLE_B];
  byte block;
  byte len;

  // wait until 30 seconds for input from serial
  Serial.setTimeout(30000L);

  // ----------------------------------------------------------------------------

  // ask personal data: Family name
  Serial.println(user_questions.last_name);

  // read Family Name from Serial Monitor
  len = Serial.readBytesUntil('\n', (char *) user_data.last_name, DOUBLE_B);

  buffer_serial_debugging(1, user_data.last_name);

  // reserve 2 blocks (32 bytes) for Family name
  write_two_block_data(1, user_data.last_name, len);

  buffer_serial_debugging(2, user_data.last_name);

  // ----------------------------------------------------------------------------

  // ask personal data: First name
  Serial.println(user_questions.first_name);

  // read First name from Serial Monitor
  len = Serial.readBytesUntil('\n', (char *) user_data.first_name, DOUBLE_B);

  buffer_serial_debugging(3, user_data.first_name);

  // reserve 2 blocks (32 bytes) for First name
  write_two_block_data(4, user_data.first_name, len);

  buffer_serial_debugging(4, user_data.first_name);

  // ----------------------------------------------------------------------------

  // ask personal data: birth date
  Serial.println(user_questions.birth_date);

  // read First name from Serial Monitor
  len = Serial.readBytesUntil('\n', (char *) user_data.birth_date, SINGLE_B);

  buffer_serial_debugging(5, user_data.birth_date);

  pad_until_dimension(user_data.birth_date, len, SINGLE_B, ' ');

  // reserve 1 block (16 bytes) for birth date
  write_one_block_data(6, user_data.birth_date, 0);

  buffer_serial_debugging(6, user_data.birth_date);

  // ----------------------------------------------------------------------------


  // if you are here, personal data have been written sucessfully
  Serial.println("#DEBUG: end of code");

  // put PICC in HALT state
  rfid.PICC_HaltA();

  // stop encryption on PCD
  rfid.PCD_StopCrypto1();
}
