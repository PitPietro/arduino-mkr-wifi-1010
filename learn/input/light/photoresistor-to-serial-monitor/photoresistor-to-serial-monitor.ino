/*
  Photoresistor Analogo Input

  Print on the Serial Monitor the value given from the photoresistor.
  Values goes from 0 to 1023 (1024 values = 2^10)
*/

#define RES_PIN A0

void setup() {
  // initialize serial communication at 9600 baud rate
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only.
  while (!Serial)
    ;
}

void loop() {
  int photoresistor_value = analogRead(RES_PIN);
  Serial.print("value: ");
  Serial.println(photoresistor_value);

  // slow down to make it human readable
  delay(100);
}
