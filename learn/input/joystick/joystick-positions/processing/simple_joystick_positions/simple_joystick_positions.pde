// import the Serial library
import processing.serial.*;

color black = #000000; // color(0, 0, 0);
color grey = color(180, 180, 180);
color white_blue = #0096c7;

int side, half_side, half_height, quarter_side;

// the number 10 is ASCII for linefeed: end of Serial.println()
int end = 10;
String serial;
Serial port;


enum position {
  NONE, // x =  510 ; y =  510
    UP, // x =  510 ; y =    0
    DOWN, // x =  510 ; y = 1023
    LEFT, // x =    0 ; y =  510
    RIGHT, // x = 1023 ; y =  510
    UP_RIGHT, // x = 1023 ; y =    0
    UP_LEFT, // x =    0 ; y =    0
    DOWN_RIGHT, // x = 1023 ; y = 1023
    DOWN_LEFT, // x =    0 ; y = 1023
};

void setup() {
  size(960, 540);
  // fullScreen();

  println("Arduino connected to port " + Serial.list()[0]);

  // initializing the object by assigning a port and baud rate (must match that of Arduino)
  port = new Serial(this, Serial.list()[0], 9600);

  // throws out the first reading, in case we started reading in the middle of a string from Arduino
  port.clear();
  serial = port.readStringUntil(end);
  serial = null;

  background(255);

  side = width/2;
  half_height = height/2;
  half_side = side/2;
  quarter_side = side/4;

  draw_base();
  draw_position_down_left(white_blue);
}

/*void serialEvent(Serial myPort) {
 try {
 int pos = int(myPort.readStringUntil('\n'));
 println("serialEvent: " + pos);
 }
 catch(RuntimeException e) {
 e.printStackTrace();
 }
 
 } */


/*
 * Docs: https://www.instructables.com/Arduino-to-Processing-Serial-Communication-withou/
 */
void draw() {
  // as long as there is data coming from serial port, read it and store it

  while (port.available() > 0) {
    serial = port.readStringUntil(end);
  }

  if (serial != null) {
    int pos = int(float(serial));
    println("position: " + pos);

    draw_base();

    switch (pos) {
    case 0:
      {
        // NONE
        draw_position_none(white_blue);
        break;
      }
    case 1:
      {
        // UP
        draw_position_up(white_blue);
        break;
      }
    case 2:
      {
        // DOWN
        draw_position_down(white_blue);
        break;
      }
    case 3:
      {
        // LEFT
        draw_position_left(white_blue);
        break;
      }
    case 4:
      {
        // RIGHT
        draw_position_right(white_blue);
        break;
      }
    case 5:
      {
        // UP_RIGH
        draw_position_up_right(white_blue);
        break;
      }
    case 6:
      {
        // UP_LEFT
        draw_position_up_left(white_blue);
        break;
      }
    case 7:
      {
        // DOWN_RIGHT
        draw_position_down_right(white_blue);
        break;
      }
    case 8:
      {
        // DOWN_LEFT
        draw_position_down_left(white_blue);
        break;
      }
    default:
      {
        // do nothing
      }
    }
  }
}

void draw_base() {
  fill(black);
  rect(side-half_side, half_height-half_side, side, side, 20);

  fill(grey);
  circle(side, half_height, side);
}

void draw_position_none(color joystick_color) {
  fill(joystick_color);
  circle(side, half_height, half_side);
}

void draw_position_up(color joystick_color) {
  fill(joystick_color);
  circle(side, half_height-quarter_side, half_side);
}

void draw_position_down(color joystick_color) {
  fill(joystick_color);
  circle(side, half_height+quarter_side, half_side);
}

void draw_position_left(color joystick_color) {
  fill(joystick_color);
  circle(side-side/4, half_height, half_side);
}

void draw_position_right(color joystick_color) {
  fill(joystick_color);
  circle(side+side/4, half_height, half_side);
}

void draw_position_up_right(color joystick_color) {
  fill(joystick_color);
  circle(side+quarter_side, half_height-quarter_side, half_side);
}

void draw_position_up_left(color joystick_color) {
  fill(joystick_color);
  circle(side-quarter_side, half_height-quarter_side, half_side);
}

void draw_position_down_right(color joystick_color) {
  fill(joystick_color);
  circle(side+quarter_side, half_height+quarter_side, half_side);
}

void draw_position_down_left(color joystick_color) {
  fill(joystick_color);
  circle(side-quarter_side, half_height+quarter_side, half_side);
}
