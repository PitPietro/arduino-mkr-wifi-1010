/**
  Test integer overflow
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial)
    ;

  unsigned short int time = 2;
  int offset = 10;

  Serial.print(time);
  Serial.print(" - ");
  Serial.print(offset);
  Serial.print(" = ");
  Serial.print(time-offset);
}

void loop() {
}