#include <Servo.h>
// #define FRONT 0

// #define REAR 180
#define STOP 80

// create servo object
Servo test_servo;

/*
   360° Continuous Servo Motors - Stop

   You can't control the angle, but the speed and the direction.

   Wiring diagram:
   - GND --> GND (black wire)
   - 5V  --> VCC (red wire)
   - D9  --> DATA IN (white wire)

   It works with both 5V and 3.3V boards.

   Docs: https://www.arduino.cc/reference/en/libraries/servo/
*/
void setup() {
  // attaches the servo on pin 9 to the servo object
  test_servo.attach(9);

  test_servo.write(STOP);
  delay(1000);
}

void loop() {
}
