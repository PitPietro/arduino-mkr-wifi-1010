#include <Servo.h>

// create servo object
Servo test_servo;

/*
   180° Servo Motors

   Wiring diagram:
   - GND --> GND (brown wire)
   - 5V  --> VCC (red wire)
   - D9  --> DATA IN (orange wire)

   It works with both 5V and 3.3V boards.

   Docs: https://www.arduino.cc/reference/en/libraries/servo/
*/
void setup() {
  // attaches the servo on pin 9 to the servo object
  test_servo.attach(9);
  Serial.begin(9600);
}

void loop() {
  for (int i = 0; i <= 180; i++) {
    test_servo.write(i);

    Serial.print("value: ");
    Serial.println(i);
    delay(50);
  }

  // test_servo.write(STOP);
  // delay(500);
}
