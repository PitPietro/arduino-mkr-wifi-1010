/*
   Bluetooth Low Energy - Led Test

   Wiring Diagram: LED connected to pin D2 with a 220 Ohm resistor

   The major steps are:
   1. Create a new service
   2. Create an LED characteristic
   3. Create an analog pin characteristic
   4. Set the name for our device
   5. Start advertising the device
   6. Create a conditional that works only if an external device is connected
   7. Create a conditional that turns on an LED over Bluetooth®
   8. Read an analog pin over Bluetooth®

   Please Note: ArduinoBLE.h library is required.
   https://github.com/arduino-libraries/ArduinoBLE

   Docs:
   1. https://docs.arduino.cc/tutorials/mkr-wifi-1010/enabling-ble (tutorial)
   2. https://www.arduino.cc/reference/en/libraries/arduinoble/ (Arduino reference)
   3. https://axodyne.com/2020/08/ble-uuids/ (UUIDs clarifier)

   GATT (General Attribute Profile)
   1. services & characteristics https://www.bluetooth.com/specifications/assigned-numbers/
   2. https://btprodspecificationrefs.blob.core.windows.net/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf
*/

#include <ArduinoBLE.h>

// from "16-bit UUID Numbers Document" document, the service below has:
// - Allocation Type: GATT Service
// - Allocated UUID: 0x180A
// - Device Information: Device Information

// create a new Bluetooth® Low Energy service
// parameter: uuid (16-bit or 128-bit UUID in String format)
// https://www.arduino.cc/reference/en/libraries/arduinoble/bleservice/
BLEService sensorService("180A");

// create the LED characteristic
// UUID for Digital Output has
// 1. assigned number: 0x2A57
// 2. Uniform Type Identifier: org.bluetooth.characteristic.digital_output

BLEByteCharacteristic LEDCharacteristic("2A57", BLERead | BLEWrite);

long previous_millis = 0;

// define the LED's pin number
const char led_pin = 2;

void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  // initialize led_pin to indicate when a central is connected
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(led_pin, OUTPUT);

  // initialize ArduinoBLE library
  if (!BLE.begin()) {
    Serial.println("Bluetooth® Low Energy failed to start!");
    while (1);
  }

  // set the name that will appear when scanning for Bluetooth® devices
  BLE.setLocalName("MKR WiFi 1010");

  // set the advertised service UUID used when advertising to the value of the BLEService provided
  BLE.setAdvertisedService(sensorService);

  // add the characteristic to the BLEService
  sensorService.addCharacteristic(LEDCharacteristic);

  // add a BLEService to the set of services the BLE device provides
  BLE.addService(sensorService);

  // set initial value for characteristic
  LEDCharacteristic.writeValue(0);

  // start advertising the BLE device
  BLE.advertise();
  Serial.println("Bluetooth® device active, waiting for connections...");
}

void loop() {
  // query the central BLE device connected: wait for a BLE central
  BLEDevice central_device = BLE.central();

  // save the Bluetooth® MAC address of the BLE device
  String central_address = central_device.address();

  // if a central is connected to the peripheral
  if (central_device) {
    Serial.print("MAC address of central: ");
    Serial.println(central_address);

    // turn on the built-in LED to indicate the connection
    digitalWrite(LED_BUILTIN, HIGH);

    // while the central is connected
    while (central_device.connected()) {
      unsigned long current_millis = millis();

      // check characteristic status every 200ms
      if (current_millis - previous_millis >= 200) {
        previous_millis = current_millis;

        // query if the characteristic value has been written by another BLE device
        if (LEDCharacteristic.written()) {
          if (LEDCharacteristic.value()) {
            Serial.println("LED on"); // any value not equal 0
            digitalWrite(led_pin, HIGH);
          } else {
            Serial.println("LED off"); // 0 value
            digitalWrite(led_pin, LOW);
          }
        }
      }
    }

    // when the central disconnects, turn off the built-in LED
    digitalWrite(LED_BUILTIN, LOW);
    Serial.print("Disconnected from central: ");
    Serial.println(central_address);
  }
}
