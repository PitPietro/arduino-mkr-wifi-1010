/*
   Bluetooth Low Energy - Battery Level

   Wiring Diagram:
   1. 10k Ohm Potentiometer connected to pin A1 (optional)

   If no potentiometer is attached, the battery level will be random

   The major steps are:
   1. Create a new service
   2. Create the battery level characteristic
   3. Set the name for the device
   4. Start advertising the device
   5. Create a conditional that works only if an external device is connected
   6. Read an analog pin over Bluetooth® and assign to battery level

   Please Note: ArduinoBLE.h library is required.
   https://github.com/arduino-libraries/ArduinoBLE

   Docs:
   1. https://docs.arduino.cc/tutorials/mkr-wifi-1010/enabling-ble (tutorial)
   2. https://www.arduino.cc/reference/en/libraries/arduinoble/ (Arduino reference)
   3. https://axodyne.com/2020/08/ble-uuids/ (UUIDs clarifier)

   GATT (General Attribute Profile)
   1. services & characteristics https://www.bluetooth.com/specifications/assigned-numbers/
   2. https://btprodspecificationrefs.blob.core.windows.net/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf
*/

#include <ArduinoBLE.h>

// from "16-bit UUID Numbers Document" document, the service below has:
// - Allocation Type: GATT Service
// - Allocated UUID: 0x180F
// - Device Information: Battery

// create a new Bluetooth® Low Energy service
// parameter: uuid (16-bit or 128-bit UUID in String format)
// https://www.arduino.cc/reference/en/libraries/arduinoble/bleservice/
BLEService batteryService("180F");

// create the battery level characteristic
// UUID for Battery Level has
// 1. assigned number: 0x2A19
// 2. Uniform Type Identifier: org.bluetooth.characteristic.battery_level

BLEUnsignedCharCharacteristic batteryLevelChar("2A19", BLERead | BLENotify);

long previous_millis = 0;

// define thepotentiometer's pin number
const char pot_pin = A1;

void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  // initialize led_pin to indicate when a central is connected
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pot_pin, INPUT);

  // initialize ArduinoBLE library
  if (!BLE.begin()) {
    Serial.println("Bluetooth® Low Energy failed to start!");
    while (1);
  }

  // set the name that will appear when scanning for Bluetooth® devices
  BLE.setLocalName("MKR WiFi 1010");

  // set the advertised service UUID used when advertising to the value of the BLEService provided
  BLE.setAdvertisedService(batteryService);

  // add the characteristic to the BLEService
  batteryService.addCharacteristic(batteryLevelChar);

  // add a BLEService to the set of services the BLE device provides
  BLE.addService(batteryService);

  // start advertising the BLE device
  BLE.advertise();
  Serial.println("Bluetooth® device active, waiting for connections...");
}

void loop() {
  // query the central BLE device connected: wait for a BLE central
  BLEDevice central_device = BLE.central();

  // save the Bluetooth® MAC address of the BLE device
  String central_address = central_device.address();

  // if a central is connected to the peripheral
  if (central_device) {
    Serial.print("MAC address of central: ");
    Serial.println(central_address);

    // turn on the built-in LED to indicate the connection
    digitalWrite(LED_BUILTIN, HIGH);

    // while the central is connected
    while (central_device.connected()) {
      unsigned long current_millis = millis();

      // check characteristic status every 200ms
      if (current_millis - previous_millis >= 200) {
        previous_millis = current_millis;

        // read values from analog port
        int pot_value = analogRead(A1);
        int batteryLevel = map(pot_value, 0, 1023, 0, 100);

        // Serial.print("Battery Level % is: ");
        // Serial.println(batteryLevel);
        batteryLevelChar.writeValue(batteryLevel);
      }
    }

    // when the central disconnects, turn off the built-in LED
    digitalWrite(LED_BUILTIN, LOW);
    Serial.print("Disconnected from central: ");
    Serial.println(central_address);
  } else {
    // try to recconnect to central

    // query the central BLE device connected: wait for a BLE central
    central_device = BLE.central();

    // save the Bluetooth® MAC address of the BLE device
    central_address = central_device.address();
  }
}
