// #include "Keyboard.h"
#include "Mouse.h"

// set pin numbers for the buttons
const char pin_up_move = 7;
const char pin_down_move = 6;
const char pin_right_move = 5;
const char pin_left_move = 4;
const char pin_right_click = 3;
const char pin_left_click = 2;

void setup() {
  // initialize inputs
  pinMode(pin_up_move, INPUT);
  pinMode(pin_down_move, INPUT);
  pinMode(pin_right_move, INPUT);
  pinMode(pin_left_move, INPUT);
  pinMode(pin_left_click, INPUT);
  pinMode(pin_right_click, INPUT);

  Serial.begin(9600);

  // initialize mouse control
  Mouse.begin();
  // Keyboard.begin();
}

void loop() {
  // use the push buttons to control the mouse move/clicks
  if (digitalRead(pin_up_move) == HIGH) {
    Serial.println("move mouse up");
    Mouse.move(0, -1);
  }
  if (digitalRead(pin_down_move) == HIGH) {
    Serial.println("move mouse down");
    Mouse.move(0, 1);
  }
  if (digitalRead(pin_left_move) == HIGH) {
    Serial.println("move mouse left");
    Mouse.move(-1, 0);
  }
  if (digitalRead(pin_right_move) == HIGH) {
    Serial.println("move mouse right");
    Mouse.move(1, 0);
  }
  if (digitalRead(pin_right_click) == HIGH) {
    Serial.println("mouse right click");
    Mouse.click(MOUSE_RIGHT);
  }
  if (digitalRead(pin_left_click) == HIGH) {
    Serial.println("mouse left click");
    Mouse.click(MOUSE_LEFT);
  }

}
