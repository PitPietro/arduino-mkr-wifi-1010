/*
  Keyboard test

  For the Arduino Leonardo, Micro or Due

  Reads a byte from the serial port, sends a keystroke back.
  The sent keystroke is one higher than what's received, e.g. if you send a,
  you get b, send A you get B, and so forth.

  The circuit:
  - none

  created 21 Oct 2011
  modified 27 Mar 2012
  by Tom Igoe

  This example code is in the public domain.

  Docs: https://www.arduino.cc/en/Tutorial/BuiltInExamples/KeyboardSerial

  You can use this example with Adafruit SAMD boards.
  Adafruit Docs: https://learn.adafruit.com/experimenters-guide-for-metro/configure-arduino-for-the-metro-express
*/

#include "Keyboard.h"

void setup() {
  // open the serial port:
  Serial.begin(9600);
  // initialize control over the keyboard:
  Keyboard.begin();
}

/*
   Foreach character sent through the Serial Monitor, the board will write the subsequentone within the machine the keyboard.
   Write a char or a whole string in Serial Monitor and press 'Enter' to send it.
*/

void loop() {
  // check for incoming serial data:
  if (Serial.available() > 0) {
    // read incoming serial data:
    char inChar = Serial.read();

    // Serial.print("User input: ");
    Serial.println(inChar);
    
    // Type the next ASCII value from what you received:
    Keyboard.write(inChar + 1);
  }
}
