#include "Keyboard.h"

// pin numbers for the buttons
const char pin_d9 = 9;
const char pin_d8 = 8;
const char pin_d7 = 7;
const char pin_d6 = 6;
const char pin_d5 = 5;
const char pin_d4 = 4;
const char pin_d3 = 3;
const char pin_d2 = 2;

// Base Octave = 3
//const char key_d9 = 't'; // G4
//const char key_d8 = 'r'; // F4
//const char key_d7 = 'e'; // E4
//const char key_d6 = 'w'; // D4
//const char key_d5 = 'q'; // C4
//const char key_d4 = 'm'; // B4
//const char key_d3 = 'n'; // A3
//const char key_d2 = 'b'; // G3

const char key_d9 = 'b'; // G1
const char key_d8 = 'g'; // F#1
const char key_d7 = 'v'; // F1
const char key_d6 = 'c'; // E1
const char key_d5 = 'd'; // D#1
const char key_d4 = 'x'; // D1
const char key_d3 = 's'; // C#1
const char key_d2 = 'z'; // C1

// avoid repeting the same code foreach button
void press_release_key(char pin_number, char key_to_use);

/*
  Keyboard control for Virtual MIDI Piano

  You need to use a board with USB native support like:
  - Arduino Due https://store.arduino.cc/products/arduino-due
  - Arduino Micro https://store.arduino.cc/products/arduino-micro
  - Arduino Leonardo https://store.arduino.cc/products/arduino-leonardo-with-headers
  - Adafruit Metro Express https://www.adafruit.com/product/3505

  The comments assume: Base Octave = 1, Transpose = 0.

  Virtual MIDI Piano Keyboard: https://vmpk.sourceforge.io/
  Links: https://www.arduino.cc/reference/en/language/functions/usb/keyboard/keyboardmodifiers/
*/
void setup() {
  // initialize inputs
  pinMode(pin_d9, INPUT);
  pinMode(pin_d8, INPUT);
  pinMode(pin_d7, INPUT);
  pinMode(pin_d6, INPUT);
  pinMode(pin_d5, INPUT);
  pinMode(pin_d4, INPUT);
  pinMode(pin_d3, INPUT);
  pinMode(pin_d2, INPUT);

  Serial.begin(9600);

  // initialize keyboard control
  Keyboard.begin();
}

/*
   use the push buttons to control the keyboard modifiers
*/
void loop() {
  press_release_key(pin_d9, key_d9);
  press_release_key(pin_d8, key_d8);
  press_release_key(pin_d7, key_d7);
  press_release_key(pin_d6, key_d6);
  press_release_key(pin_d5, key_d5);
  press_release_key(pin_d4, key_d4);
  press_release_key(pin_d3, key_d3);
  press_release_key(pin_d2, key_d2);
  
  delay(10);
}

/*
 * Press and release a key when the corresponding button is pressed 
 */
void press_release_key(char pin_number, char key_to_use) {
  if (digitalRead(pin_number) == HIGH) {
    Keyboard.press(key_to_use);

    Serial.print("key pressed: ");
    Serial.println(key_to_use);
  } else {
    Keyboard.release(key_to_use);
  }

}
