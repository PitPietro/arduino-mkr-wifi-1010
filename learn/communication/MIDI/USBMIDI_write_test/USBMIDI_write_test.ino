/*
   USBMIDI Write Test

   Tested on VMPK: Virtual MIDI Piano Keyboard
*/

#include "MIDIUSB.h"

// First parameter is the event type (0x09 = note on, 0x08 = note off).
// Second parameter is note-on/note-off, combined with the channel.
// Channel can be anything between 0-15. Typically reported to the user as 1-16.
// Third parameter is the note number (48 = middle C).
// Fourth parameter is the velocity (64 = normal, 127 = fastest).

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

void setup() {
  Serial.begin(115200);
  Serial.println("USBMIDI Write Test\n");
}

// First parameter is the event type (0x0B = control change).
// Second parameter is the event type, combined with the channel.
// Third parameter is the control number number (0-119).
// Fourth parameter is the control value (0-127).

void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}

void loop() {
  // Channel 0, middle C, normal velocity
  writeSingleNote(0, 48, 64); // C3
  writeSingleNote(0, 49, 64); // C#3
  writeSingleNote(0, 50, 64); // D3
  writeSingleNote(0, 51, 64); // D#3
  writeSingleNote(0, 52, 64); // E3
  writeSingleNote(0, 53, 64); // F3
  writeSingleNote(0, 54, 64); // F#3
  writeSingleNote(0, 55, 64); // G3
  writeSingleNote(0, 56, 64); // G#3
  writeSingleNote(0, 57, 64); // A3
  writeSingleNote(0, 58, 64); // A#3
  writeSingleNote(0, 59, 64); // B3
  writeSingleNote(0, 60, 64); // C4

  // controlChange(0, 10, 65); // Set the value of controller 10 on channel 0 to 65
}

void writeSingleNote(byte channel, byte pitch, byte velocity) {
  Serial.print("Sending note on: ");
  Serial.println(pitch);
  noteOn(channel, pitch, velocity);
  MidiUSB.flush();
  delay(500);

  Serial.print("Sending note off: ");
  Serial.println(pitch);

  noteOff(channel, pitch, velocity);
  MidiUSB.flush();
  delay(500);
}
