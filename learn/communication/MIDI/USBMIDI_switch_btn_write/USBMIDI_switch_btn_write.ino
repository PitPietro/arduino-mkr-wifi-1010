/*
   USBMIDI Write when Switch button is pressed

   Tested on VMPK: Virtual MIDI Piano Keyboard
*/

#include "MIDIUSB.h"

// --- defaults ---
const char ch = 0; // default channel
const char sp = 64; // defulat speed

// --- buttons pins --- really needed?
const char btn0pin = 0;
const char btn1pin = 1;
const char btn2pin = 2;
const char btn3pin = 3;
const char btn4pin = 4;
const char btn5pin = 5;
const char btn6pin = 6;
const char btn7pin = 7;
const char btn8pin = 8;
const char btn9pin = 9;
const char btn10pin = 10;
// could not managed to use pins 11 (SDA) and 12 (SCL)
const char btn11pin = 13;

// --- buttons state & previous state ---
char btn0state = 0;
char btn0prev_state = 0;
char btn1state = 0;
char btn1prev_state = 0;
char btn2state = 0;
char btn2prev_state = 0;
char btn3state = 0;
char btn3prev_state = 0;
char btn4state = 0;
char btn4prev_state = 0;
char btn5state = 0;
char btn5prev_state = 0;
char btn6state = 0;
char btn6prev_state = 0;
char btn7state = 0;
char btn7prev_state = 0;
char btn8state = 0;
char btn8prev_state = 0;
char btn9state = 0;
char btn9prev_state = 0;
char btn10state = 0;
char btn10prev_state = 0;
char btn11state = 0;
char btn11prev_state = 0;

const int btn0pitch = 48;
const int btn1pitch = 49;
const int btn2pitch = 50;
const int btn3pitch = 51;
const int btn4pitch = 52;
const int btn5pitch = 53;
const int btn6pitch = 54;
const int btn7pitch = 55;
const int btn8pitch = 56;
const int btn9pitch = 57;
const int btn10pitch = 58;
const int btn11pitch = 59;

/**
 * midiEventPacket_t parameters are:
 * 1) event type (0x09 = note on, 0x08 = note off)
 * 2) note-on/note-off, combined with the channel
 * 3) note number (48 = middle C)
 * 4) velocity (64 = normal, 127 = fastest)
 * 
 * Channel can be anything between 0-15.
 * Typically reported to the user as 1-16.
*/
void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

void setup() {
  Serial.begin(115200);

  pinMode(btn0pin, INPUT);
  pinMode(btn1pin, INPUT);
  pinMode(btn2pin, INPUT);
  pinMode(btn3pin, INPUT);
  pinMode(btn4pin, INPUT);
  pinMode(btn5pin, INPUT);
  pinMode(btn6pin, INPUT);
  pinMode(btn7pin, INPUT);
  pinMode(btn8pin, INPUT);
  pinMode(btn9pin, INPUT);
  pinMode(btn10pin, INPUT);
  pinMode(btn11pin, INPUT);
}

void loop() {
  // read the state of buttons values
  btn0state = digitalRead(btn0pin);
  btn1state = digitalRead(btn1pin);
  btn2state = digitalRead(btn2pin);
  btn3state = digitalRead(btn3pin);
  btn4state = digitalRead(btn4pin);
  btn5state = digitalRead(btn5pin);
  btn6state = digitalRead(btn6pin);
  btn7state = digitalRead(btn7pin);
  btn8state = digitalRead(btn8pin);
  btn9state = digitalRead(btn9pin);
  btn10state = digitalRead(btn10pin);
  btn11state = digitalRead(btn11pin);

  btnController(btn0state, btn0prev_state, btn0pitch);
  btnController(btn1state, btn1prev_state, btn1pitch);
  btnController(btn2state, btn2prev_state, btn2pitch);
  btnController(btn3state, btn3prev_state, btn3pitch);
  btnController(btn4state, btn4prev_state, btn4pitch);
  btnController(btn5state, btn5prev_state, btn5pitch);
  btnController(btn6state, btn6prev_state, btn6pitch);
  btnController(btn7state, btn7prev_state, btn7pitch);
  btnController(btn8state, btn8prev_state, btn8pitch);
  btnController(btn9state, btn9prev_state, btn9pitch);
  btnController(btn10state, btn10prev_state, btn10pitch);
  btnController(btn11state, btn11prev_state, btn11pitch);

  // delay to avoid bouncing
  delay(10);

  // save the current states as the last states, for next time through the loop
  btn0prev_state = btn0state;
  btn1prev_state = btn1state;
  btn2prev_state = btn2state;
  btn3prev_state = btn3state;
  btn4prev_state = btn4state;
  btn5prev_state = btn5state;
  btn6prev_state = btn6state;
  btn7prev_state = btn7state;
  btn8prev_state = btn8state;
  btn9prev_state = btn9state;
  btn10prev_state = btn10state;
  btn11prev_state = btn11state;
}

void btnController(char state, char prev_state, int pitch) {
  if (state != prev_state) {
    if (state == HIGH) {
      Serial.print(">>> TEST: sending note on: ");
      Serial.println(pitch);

      noteOn(ch, pitch, sp);
      MidiUSB.flush();
    } else {
      Serial.print(">>> TEST: sending note off: ");
      Serial.println(pitch);

      noteOff(ch, pitch, sp);
      MidiUSB.flush();
    }
  }
}
