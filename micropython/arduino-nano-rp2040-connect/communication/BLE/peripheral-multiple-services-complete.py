import time
import struct
import bluetooth
from random import randint
from machine import Pin
from micropython import const
from ble_advertising import advertising_payload

# full list of IRQ event codes available in 'peripheral-multiple-services.py' script
_IRQ_CENTRAL_CONNECT    = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_WRITE        = const(3)
_IRQ_GATTS_READ_REQUEST = const(4)
_IRQ_CONNECTION_UPDATE  = const(27)

_BATTERY_SERVICE_UUID = bluetooth.UUID(0x180F)

# Uniform Type Identifier: org.bluetooth.characteristic.battery_level
_BATTERY_CHAR_UUID = (bluetooth.UUID(0x2A19), bluetooth.FLAG_READ | bluetooth.FLAG_NOTIFY)
_BATTERY_SERVICE = (_BATTERY_SERVICE_UUID, (_BATTERY_CHAR_UUID,),)

_CUSTOM_SERVICE_UUID = bluetooth.UUID(0x2907)

# org.bluetooth.characteristic.string
_USER_STRING = (bluetooth.UUID(0x2A3D), bluetooth.FLAG_READ | bluetooth.FLAG_WRITE | bluetooth.FLAG_NOTIFY)
_USER_HEIGHT = (bluetooth.UUID(0x2A8E), bluetooth.FLAG_WRITE)

_USER_SERVICE = (_CUSTOM_SERVICE_UUID, (_USER_HEIGHT, _USER_STRING,),)
SERVICES = (_BATTERY_SERVICE, _USER_SERVICE)
SERVICES_UUIDs = [_BATTERY_SERVICE_UUID, _CUSTOM_SERVICE_UUID]


'''
Battery Service has read and notify flags.
User Height Service has write flag.
User String has write, read and notify flags.

self._handler handles all the characteristics that have write flag.
self._char_buffers holds a bytearray buffer for each of those characteristics.
'''
class BLEMultipleServicesComplete:
    def __init__(self, ble, name="Nano RP2040"):
        self._ble = ble
        self._ble.active(True)
        self._ble.irq(self._irq)

        # hold a bytearray buffer for each characteristic that can perform write operation
        self._char_buffers = [bytearray(), bytearray()]

        # docs: https://docs.micropython.org/en/latest/library/bluetooth.html#bluetooth.BLE.gatts_register_services
        ((self._battery,), (self._usr_height, self._usr_string), ) = self._ble.gatts_register_services(SERVICES)
        self._connections = set()
        self._handler = None
        self._payload = advertising_payload(name=name, services=SERVICES_UUIDs)
        self._advertise()

    def irq(self, handler):
        self._handler = handler

    def _irq(self, event, data):
        if event == _IRQ_CENTRAL_CONNECT: # a central has connected to this peripheral
            conn_handle, addr_type, addr = data
            self._connections.add(conn_handle)

            print('\nCentral connected: event code = {}'.format(event))
            print('\t conn_handle = {}, addr_type = {}, addr = {}'.format(conn_handle, addr_type, bytes(addr)))
        elif event == _IRQ_CENTRAL_DISCONNECT: # a central has disconnected from this peripheral
            conn_handle, addr_type, addr = data
            self._connections.remove(conn_handle)

            print('\nCentral disconnected: event code = {}\nWaiting for other devices to connect...'.format(event))
            print('\t conn_handle = {}, addr_type = {}, addr = {}'.format(conn_handle, addr_type, bytes(addr)))

            # start advertising again to allow a new connection
            self._advertise()
        elif event == _IRQ_GATTS_WRITE: # a client has written to this characteristic or descriptor
            conn_handle, attr_handle = data

            print('\nGATTS write: event code = {}'.format(event))
            print('\t conn_handle = {}, attr_handle = {}'.format(conn_handle, attr_handle))

            if conn_handle in self._connections:
                if attr_handle == self._usr_height:
                    self._char_buffers[0] += self._ble.gatts_read(self._usr_height)
                    print("\t write to _usr_height has been performed")
                    if self._handler:
                        # call the handler with the appropriate message and index
                        self._handler(msg="user height: ", index=0)
                elif attr_handle == self._usr_string:
                    self._char_buffers[1] += self._ble.gatts_read(self._usr_string)
                    print("\t write to _usr_string has been performed")
                    if self._handler:
                        # call the handler with the appropriate message and index
                        self._handler(msg="user string: ", index=1)
        elif event == _IRQ_GATTS_READ_REQUEST: # a client has issued a read (only supported on STM3)
            # return a non-zero integer to deny the read (see below), or zero (or None) to accept the read
            conn_handle, attr_handle = data

            print('\nGATT read (request): event code = {}'.format(event))
            print('\t conn_handle = {}, attr_handle = {}'.format(conn_handle, attr_handle))
        elif event == _IRQ_CONNECTION_UPDATE: # remote device has updated connection parameters
            conn_handle, conn_interval, conn_latency, supervision_timeout, status = data

            print('\nRemote device updated connection parameters: event code = {}'.format(event))
            print('\t conn_handle = {}, conn_interval = {}, conn_latency = {}, supervision_timeout = {}, status = {}'.format(conn_handle, conn_interval, conn_latency, supervision_timeout, status))

    def _advertise(self, interval_us=50000):
        self._ble.gap_advertise(interval_us, adv_data=self._payload)

    '''
    Data is sint16 in percentage with a resolution of 0.01%
    Write the local value, ready for a central to read
    '''
    def set_battery_level(self, level_c, notify=False):
        battery_level = struct.pack('<h', int(level_c * 100))
        self._ble.gatts_write(self._battery, battery_level)
        if notify:
            for conn_handle in self._connections:
                # notify all connected centrals to issue a read
                self._ble.gatts_notify(conn_handle, self._battery)

    '''
    Pass as parameter the index of the buffer
    '''
    def read_char_buffer(self, index, sz=None):
        if not sz:
            sz = len(self._char_buffers[index])
        result = self._char_buffers[index][0:sz]
        self._char_buffers[index] = self._char_buffers[index][sz:]
        return result


def demo():
    ble = bluetooth.BLE()
    ble_device = BLEMultipleServicesComplete(ble)

    '''
    Single handler for any characteristic that wants to display what has been wrote
    '''
    def on_char_write(msg, index):
        print(msg, ble_device.read_char_buffer(index).decode().strip())

    ble_device.irq(handler=on_char_write)

    random_int = 0
    i = 0

    while True:
        # write every second, notify every 10 seconds
        i = (i + 1) % 10
        ble_device.set_battery_level(random_int, notify=i == 0)
        # ble_device.set_battery_level(random_int) # uncomment this line and comment the one above to stop notify for debugging reason

        # change the battery level with a random value
        random_int = randint(1, 100)
        time.sleep_ms(1000)



'''
Docs:
  1. https://github.com/micropython/micropython/blob/master/examples/bluetooth/ble_uart_peripheral.py
  2. https://docs.micropython.org/en/latest/library/bluetooth.html
  3. https://axodyne.com/2020/08/ble-uuids/ (UUIDs clarifier)

GATT (General Attribute Profile)
  1. services & characteristics https://www.bluetooth.com/specifications/assigned-numbers/
  2. https://btprodspecificationrefs.blob.core.windows.net/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf
'''
if __name__ == "__main__":
    demo()
