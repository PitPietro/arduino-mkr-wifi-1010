# MQTT Hello World

## Goal
The Arduino MKR subscribe to `/hello` topic and listen for incoming messages. If the message's payload is equal `"open"`, the Arduino turn its built-in LED **ON** and move a servo motor to position `0`. Else, if the payload is equal `"close"`, the board turn its built-in LED **OFF** and move the given servo motor to position `180`.

At the same time, the Arduino board publish to `/hello` topic the payload `"world"` once every a given period of time.

You can also send `"open"` and `"close"` payloads through the **UI** available at [`http://localhost:1880/ui`](http://localhost:1880/ui), which can also be accessed by any device connected to the same network of the host machine. The user interface is based on Angular framework: it's compatible with mobile device screen dimensions.

![dashboard](./assets/dashboard-screen.png)

## Local Node-RED installation
Follow [Running Node-RED locally](https://nodered.org/docs/getting-started/local) page to install [Node-RED](https://nodered.org/) locally.

You need to run the following commands on the machine to install Node-RED:
```bash
$ sudo npm install -g --unsafe-perm node-red
$ node-red # run and stop to let create the config files
$ cd ./.node-red
$ npm i node-red-contrib-mqtt-broker # https://flows.nodered.org/node/node-red-contrib-mqtt-broker
$ npm i node-red-dashboard # https://flows.nodered.org/node/node-red-dashboard
# $ npm i node-red-node-arduino # https://flows.nodered.org/node/node-red-node-arduino (not necessary for this project)
```

You can directly import the flow with `Ctrl` + `I` or by clicking **Import** inside Node-RED menu.

Install local **MQTT broker**:

```bash
$ sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa
$ sudo apt update
$ sudo apt install -y mosquitto mosquitto-clients
$ sudo apt clean
```

Starting with the release of **mosquitto** version 2.0.0, the default configuration will only bind to `localhost` as a move to a more  secure default posture. If you want to be able to access the broker from other machines, you will need to explicitly edit the configuration files to either add a new listener that binds to the external IP address or add a bind entry for the default listener.

In `/etc/mosquitto/mosquitto.conf` set:

```txt
listener 1883
allow_anonymous true
```

Then, to start the **broker**, use the following command:

```bash
$ sudo mosquitto -c /etc/mosquitto/mosquitto.conf
```

## Static IP address
To avoid having to modify the `arduino-secrets.h` file every time you shutdown your machine, I suggest you to set up a local static IP address.

On a Linux machine with KDE as *desktop environment*, you can right click on Internet icon and press `Configure Network Connections...` settings option. Then in `IPv4` page, set **Method** as `Manual` and a new item as shown below:
![static IP on KDE](./assets/static-IP.png)

As you can see, to choose the right address for the **Gateway** (and **DNS Servers**) you need to open a terminal and type:

```bash
$ nmcli d # show device list
DEVICE             TYPE      STATE         CONNECTION      
wlp0s20f3          wifi      connected     FASTWEB-80F0D6
# ...

$ ip route # show networks information
default via 192.168.1.254 dev wlp0s20f3 proto dhcp metric 600 
# ...
```

This is your gateway's IP address. Then choose an arbitrary IP address for your machine, like `192.168.1.57` or `192.168.52.27` and apply the changes.

Disconnect and connect back to the network to get everything done properly.

## The flow

![flow screen](./assets/log1.png)