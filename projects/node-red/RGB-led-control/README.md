# MQTT RGB LED Control

## Goal
The Arduino MKR subscribe to `/rgb` topic and listen for incoming messages. It takes the haxedecimal color string and subdivide it into the three LED components (red, green and blue). Then, it converts the three haxadecimal values into integer value from `0` to `255` to properly write over the RGB LED pins.

Note that the built-in RGB LED is connected to digital pins `25` (green), `26` (red), `27` (blue).
`utility/wifi_drv.h` library is used to access the LED as shown in [Accessing the Built-in RGB LED on the MKR WiFi 1010](https://docs.arduino.cc/tutorials/mkr-wifi-1010/built-in-rgb) blog post.

You can set the LED color using the <u>color picker</u> placed inside the interface available at [`http://localhost:1880/ui`](http://localhost:1880/ui), which can also be accessed by any device connected to the same network of the host machine. The user interface is based on Angular framework: it's compatible with mobile device screen dimensions.

![dashboard 1](./assets/dashboard-mobile-1.jpg)

![dashboard 2](./assets/dashboard-mobile-2.jpg)

## Local Node-RED installation
Follow [Running Node-RED locally](https://nodered.org/docs/getting-started/local) page to install [Node-RED](https://nodered.org/) locally.

You need to run the following commands on the machineto install Node-RED:
```bash
$ sudo npm install -g --unsafe-perm node-red
$ cd ./.node-red
$ npm i node-red-contrib-mqtt-broker # https://flows.nodered.org/node/node-red-contrib-mqtt-broker
$ npm i node-red-dashboard # https://flows.nodered.org/node/node-red-dashboard
# $ npm i node-red-node-arduino # https://flows.nodered.org/node/node-red-node-arduino (not necessary for this project)
```

You can directly import the flow with `Ctrl` + `I` or by clicking **Import** inside Node-RED menu.

## Static IP address
To avoid having to modify the `arduino-secrets.h` file everytime you shutdown your machine, I suggest you to set up a local static IP address.

On a Linux machine with KDE as *desktop environment*, you can right click on Internet icon and press `Configure Network Connections...` settings option. Then in `IPv4` page, set **Method** as `Manual` and a new item as shown below:
![static IP on KDE](./assets/static-IP.png)

As you can see, to choose the right address for the **Gateway** (and **DNS Servers**) you need to open a terminal and type:

```bash
$ nmcli d # show device list
DEVICE             TYPE      STATE         CONNECTION      
wlp0s20f3          wifi      connected     FASTWEB-80F0D6
# ...

$ ip route # show networks information
default via 192.168.1.254 dev wlp0s20f3 proto dhcp metric 600 
# ...
```

This is your gateway's IP address. Then choose an arbitrary IP address for your machine, like `192.168.1.57` or `192.168.52.27` and apply the changes.

Disconnect and connect back to the network to get everything done properly.

## The flow

![flow screen](./assets/flow-screen.png)