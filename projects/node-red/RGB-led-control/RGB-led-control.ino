/*
   Node-RED - MQTT RGB LED Control

   Please read the README.md file placed in the same directory of this file.

   Open Node-RED and import flows.json file.

   The built-in RGB LED is connected to digital pins 25 (green), 26 (red), 27 (blue).

   Press Ctrl + Shift + N and create 'arduino-secrets.h' file as follow:

   ```c
   #define WIFI_SSID "" // place your WiFi name inside the quotes
   #define WIFI_PWD "" // place your WiFi password inside the quotes
   #define BROKER_IP "192.168.x.x" // place MQTT Broker IP address inside the quotes
   #define DEV_NAME  "" // place MQTT Client ID name inside the quotes
   #define MQTT_USER "" // place MQTT username inside the quotes (leave empty if MQTT is unauthenticated)
   #define MQTT_PWD  "" // place MQTT password inside the quotes (leave empty if MQTT is unauthenticated)
   ```

   Note: 'arduino-secrets.h' files are indicated in '.gitignore' to avoid loose of sensitive data.
*/
#include <WiFiNINA.h> // https://www.arduino.cc/en/Reference/WiFiNINA
#include <Servo.h>    // https://www.arduino.cc/reference/en/libraries/servo/
#include <MQTT.h>     // https://www.arduino.cc/reference/en/libraries/mqtt/
#include <utility/wifi_drv.h>
#include "arduino-secrets.h"

MQTTClient mqtt_client;
WiFiClient wifi_client;

// check is have been past more than 'delay_time' milliseconds
unsigned long last_millis = 0;

// MQTT broker port is 1883 by default, but you can change it
const short int broker_port = 1883;

// check /rgbtopic every 'delay_time' milliseconds
const short int delay_time = 2000;

// RGB LED pins
const char red_pin = 26;
const char green_pin = 25;
const char blue_pin = 27;
const char color_dim = 3;
const char topic_name[5] = "/rgb";

// Wifi radio's status
int status = WL_IDLE_STATUS;

void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  // check for the WiFi module
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    while (true); // do not continue
  }

  // attempt to connect to Wifi network
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to network: ");
    Serial.println(WIFI_SSID);

    // connect to WPA/WPA2 network
    status = WiFi.begin(WIFI_SSID, WIFI_PWD);

    // wait for connection
    delay(5000);
  }

  // the board is now connected, print out the data
  Serial.println("You're connected to the network");
  print_wifi_status();

  // MQTT brokers usually use port 8883 for secure connections
  mqtt_client.begin(BROKER_IP, broker_port, wifi_client);
  mqtt_client.onMessage(message_handler);
  connect();
}

void loop() {
  mqtt_client.loop();
  if (!mqtt_client.connected()) {
    connect();
  }
}

void connect() {
  Serial.print("Checking WiFi Status");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("#");
    delay(1000);
  }
  Serial.print("\nConnecting to MQTT queue");

  // try to connect to MQTT queue every second
  while (!mqtt_client.connect(DEV_NAME, MQTT_USER, MQTT_PWD)) {
    Serial.print("#");
    delay(1000);
  }

  char msg[30];
  sprintf(msg, "\nConnected to %s", BROKER_IP);
  Serial.println(msg);


  // subscribe to topic /rgb
  mqtt_client.subscribe(topic_name);
}

void message_handler(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
  if (topic == topic_name) {
    char red_chars[color_dim];
    char green_chars[color_dim];
    char blue_chars[color_dim];

    payload.substring(0, 2).toCharArray(red_chars, color_dim);
    payload.substring(2, 4).toCharArray(green_chars, color_dim);
    payload.substring(4, 6).toCharArray(blue_chars, color_dim);

    int red_value = strtoul(red_chars, NULL, 16);
    int green_value = strtoul(green_chars, NULL, 16);
    int blue_value = strtoul(blue_chars, NULL, 16);

    char msg[50];
    sprintf(msg, "red: %s (%d) | green: %s (%d) | blue: %s (%d)", red_chars, red_value, green_chars, green_value, blue_chars, blue_value);
    Serial.println(msg);

    WiFiDrv::analogWrite(green_pin, green_value);
    WiFiDrv::analogWrite(red_pin, red_value);
    WiFiDrv::analogWrite(blue_pin, blue_value);
  }
}

void print_wifi_status() {
  Serial.println("Board Information:");

  // print the board's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  Serial.println();
  Serial.println("Network Information:");
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.println(encryption, HEX);
  Serial.println();
}
