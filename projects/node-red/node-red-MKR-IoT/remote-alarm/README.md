# Remote Alarm

## Goal

## Local Node-RED installation
Follow [Running Node-RED locally](https://nodered.org/docs/getting-started/local) page to install [Node-RED](https://nodered.org/) locally.

You need to run the following commands on the machineto install Node-RED:
```bash
$ sudo npm install -g --unsafe-perm node-red
$ cd ./.node-red
$ npm i node-red-contrib-mqtt-broker # https://flows.nodered.org/node/node-red-contrib-mqtt-broker
$ npm i node-red-dashboard # https://flows.nodered.org/node/node-red-dashboard
$ npm install node-red-contrib-loop-processing # https://flows.nodered.org/node/node-red-contrib-loop-processing
# $ npm i node-red-node-arduino # https://flows.nodered.org/node/node-red-node-arduino (not necessary for this project)
```

You can directly import the flow with `Ctrl` + `I` or by clicking **Import** inside Node-RED menu.


## The flow

<!-- ![flow screen](./assets/flow-screen.png) -->