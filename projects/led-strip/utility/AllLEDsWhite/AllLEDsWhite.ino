#include <Adafruit_NeoPixel.h>
#define PIN 6
#define NUM_LEDS 100 // 60 LEDs/meter * 5 meters
// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

/**
   Utility method to reset all the LEDs
*/
void setup() {
  strip.begin();
  // initialize all pixels to 'off'
  strip.show();
  strip.setBrightness(255);
  setAll(0xff, 0xff, 0xff);
  delay(200);
}

void loop() {

}

/* utility methods */

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++ ) {
    setPixel(i, red, green, blue);
  }
  strip.show();
}

void setPixel(int pixelPosition, byte red, byte green, byte blue) {
  strip.setPixelColor(pixelPosition, strip.Color(red, green, blue));
}
