#include <Adafruit_NeoPixel.h>

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);


void setup() {
  // initialize the strip
  strip.begin();
  // initialize all pixels to 'off'
  strip.show();
  // brightness value is in a range from 0 to 255
  strip.setBrightness(255);
}

void loop() {
  Scanner(0xff, 0, 0, 4, 10, 50);
  Scanner(0xff, 0xaa, 0, 2, 20, 100);
  Scanner(0, 0xaa, 0xff, 10, 5, 15);
}

void Scanner(byte red, byte green, byte blue, int eyeSize, int speedDelay, int returnDelay) {
  for (int i = 0; i < NUM_LEDS - eyeSize - 2; i++) {
    setAll(0, 0, 0);
    setPixel(i, red / 10, green / 10, blue / 10);
    for (int j = 1; j <= eyeSize; j++) {
      setPixel(i + j, red, green, blue);
    }
    setPixel(i + eyeSize + 1, red / 10, green / 10, blue / 10);
    showStrip();
    delay(speedDelay);
  }

  delay(returnDelay);

  for (int i = NUM_LEDS - eyeSize - 2; i > 0; i--) {
    setAll(0, 0, 0);
    setPixel(i, red / 10, green / 10, blue / 10);
    for (int j = 1; j <= eyeSize; j++) {
      setPixel(i + j, red, green, blue);
    }
    setPixel(i + eyeSize + 1, red / 10, green / 10, blue / 10);
    showStrip();
    delay(speedDelay);
  }

  delay(returnDelay);
}

void showStrip() {
  strip.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  strip.setPixelColor(pixelNumber, strip.Color(red, green, blue));
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
