#include "FastLED.h"

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100
// brightness threshold
#define BRIGHT 255

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
}

/**
 * This effect follows this pattern:
 * 1) <---------
 * 2) --------->
 * 3) ----><----
 * 4) <---------
 * 5) --------->
 * 6) <---------
 * 7) ----><----
 * 8) <-------->
 * 
 * Docs: https://www.instructables.com/The-KITT-duino-DIY-Larson-Scanner-with-an-Arduino/
 */
void loop() {
  NewKITT(0xbb, 0xcc, 0xdd, 4, 15, 50);
}

void NewKITT(byte red, byte green, byte blue, int eyeSize, int speedDelay, int returnDelay) {
  rightToLeft(red, green, blue, eyeSize, speedDelay, returnDelay);
  leftToRight(red, green, blue, eyeSize, speedDelay, returnDelay);
  outsideToCenter(red, green, blue, eyeSize, speedDelay, returnDelay);
  centerToOutside(red, green, blue, eyeSize, speedDelay, returnDelay);
  leftToRight(red, green, blue, eyeSize, speedDelay, returnDelay);
  rightToLeft(red, green, blue, eyeSize, speedDelay, returnDelay);
  outsideToCenter(red, green, blue, eyeSize, speedDelay, returnDelay);
  centerToOutside(red, green, blue, eyeSize, speedDelay, returnDelay);
}

void centerToOutside(byte red, byte green, byte blue, int eyeSize, int speedDelay, int returnDelay) {
  for (int i = ((NUM_LEDS - eyeSize) / 2); i >= 0; i--) {
    setAll(0, 0, 0);

    setPixel(i, red / 10, green / 10, blue / 10);
    for (int j = 1; j <= eyeSize; j++) {
      setPixel(i + j, red, green, blue);
    }
    setPixel(i + eyeSize + 1, red / 10, green / 10, blue / 10);

    setPixel(NUM_LEDS - i, red / 10, green / 10, blue / 10);
    for (int j = 1; j <= eyeSize; j++) {
      setPixel(NUM_LEDS - i - j, red, green, blue);
    }
    setPixel(NUM_LEDS - i - eyeSize - 1, red / 10, green / 10, blue / 10);

    showStrip();
    delay(speedDelay);
  }
  delay(returnDelay);
}

void outsideToCenter(byte red, byte green, byte blue, int eyeSize, int speedDelay, int returnDelay) {
  for (int i = 0; i <= ((NUM_LEDS - eyeSize) / 2); i++) {
    setAll(0, 0, 0);

    setPixel(i, red / 10, green / 10, blue / 10);
    for (int j = 1; j <= eyeSize; j++) {
      setPixel(i + j, red, green, blue);
    }
    setPixel(i + eyeSize + 1, red / 10, green / 10, blue / 10);

    setPixel(NUM_LEDS - i, red / 10, green / 10, blue / 10);
    for (int j = 1; j <= eyeSize; j++) {
      setPixel(NUM_LEDS - i - j, red, green, blue);
    }
    setPixel(NUM_LEDS - i - eyeSize - 1, red / 10, green / 10, blue / 10);

    showStrip();
    delay(speedDelay);
  }
  delay(returnDelay);
}

void leftToRight(byte red, byte green, byte blue, int eyeSize, int speedDelay, int returnDelay) {
  for (int i = 0; i < NUM_LEDS - eyeSize - 2; i++) {
    setAll(0, 0, 0);
    setPixel(i, red / 10, green / 10, blue / 10);
    for (int j = 1; j <= eyeSize; j++) {
      setPixel(i + j, red, green, blue);
    }
    setPixel(i + eyeSize + 1, red / 10, green / 10, blue / 10);
    showStrip();
    delay(speedDelay);
  }
  delay(returnDelay);
}

void rightToLeft(byte red, byte green, byte blue, int eyeSize, int speedDelay, int returnDelay) {
  for (int i = NUM_LEDS - eyeSize - 2; i > 0; i--) {
    setAll(0, 0, 0);
    setPixel(i, red / 10, green / 10, blue / 10);
    for (int j = 1; j <= eyeSize; j++) {
      setPixel(i + j, red, green, blue);
    }
    setPixel(i + eyeSize + 1, red / 10, green / 10, blue / 10);
    showStrip();
    delay(speedDelay);
  }
  delay(returnDelay);
}

void showStrip() {
  FastLED.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  leds[pixelNumber].r = red;
  leds[pixelNumber].g = green;
  leds[pixelNumber].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
