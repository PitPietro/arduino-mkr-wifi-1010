#include "FastLED.h"

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100
// brightness threshold
#define BRIGHT 255

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
}

void loop() {
  rainbowCycle(20);
}

void rainbowCycle(int speedDelay) {
  byte* c;
  uint16_t i, j;

  // 5 cycles of all colors on wheel
  for (j = 0; j < 256 * 5; j++) {
    for (i = 0; i < NUM_LEDS; i++) {
      c = wheel(((i * 256 / NUM_LEDS) + j) & 255);
      setPixel(i, *c, *(c + 1), *(c + 2));
    }
    showStrip();
    delay(speedDelay);
  }
}

byte* wheel(byte wheelPosition) {
  static byte c[3];

  if (wheelPosition < 85) {
    c[0] = wheelPosition * 3;
    c[1] = 255 - wheelPosition * 3;
    c[2] = 0;
  } else if (wheelPosition < 170) {
    wheelPosition -= 85;
    c[0] = 255 - wheelPosition * 3;
    c[1] = 0;
    c[2] = wheelPosition * 3;
  } else {
    wheelPosition -= 170;
    c[0] = 0;
    c[1] = wheelPosition * 3;
    c[2] = 255 - wheelPosition * 3;
  }

  return c;
}

void showStrip() {
  FastLED.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  leds[pixelNumber].r = red;
  leds[pixelNumber].g = green;
  leds[pixelNumber].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
