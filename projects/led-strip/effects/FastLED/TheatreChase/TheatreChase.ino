#include "FastLED.h"

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100
// brightness threshold
#define BRIGHT 255

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
}

/**
 * With this effect LEDs are chasing each other like what you'd see in an old Theatre.
 */
void loop() {
  theaterChase(0, 0xbb, 0xcc, 50);
}

void theaterChase(byte red, byte green, byte blue, int speedDelay) {
  for (int j = 0; j < 10; j++) {  //do 10 cycles of chasing
    for (int q = 0; q < 3; q++) {
      for (int i = 0; i < NUM_LEDS; i = i + 3) {
        // turn every third pixel on
        setPixel(i + q, red, green, blue);
      }
      showStrip();
      delay(speedDelay);

      for (int i = 0; i < NUM_LEDS; i = i + 3) {
        // turn every third pixel off
        setPixel(i + q, 0, 0, 0);
      }
    }
  }
}

void showStrip() {
  FastLED.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  leds[pixelNumber].r = red;
  leds[pixelNumber].g = green;
  leds[pixelNumber].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
