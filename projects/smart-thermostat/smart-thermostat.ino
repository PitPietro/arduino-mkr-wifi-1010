#include "Arduino_BHY2.h"

Sensor temperature(SENSOR_ID_TEMP);
SensorBSEC bsec(SENSOR_ID_BSEC);

/*
  Sensor barometer(SENSOR_ID_BARO);
  Sensor humidity(SENSOR_ID_HUM);
  Sensor gas(SENSOR_ID_GAS);
*/
/*
  Sensor temperatureWakeUp(SENSOR_ID_TEMP_WU);
  Sensor barometerWakeUp(SENSOR_ID_BARO_WU);
  Sensor humidityWakeUp(SENSOR_ID_HUM_WU);
  Sensor gasWakeUp(SENSOR_ID_GAS_WU);
*/


/**
   Nicla Sense ME Sensor Thermostat


   Website: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();

  temperature.begin();
  bsec.begin();

  /*barometer.begin();
    humidity.begin();
    gas.begin();

    temperatureWakeUp.begin();
    barometerWakeUp.begin();
    humidityWakeUp.begin();
    gasWakeUp.begin();*/
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 10) {
    lastCheck = millis();

    printSensor("temp", temperature.value());
    printSensor("compensated_temp", bsec.comp_t());
    printSensor("accuracy", bsec.accuracy());
    /*
      printSensor("barometer", barometer.value());
      printSensor("humidity", humidity.value());
      printSensor("gas", gas.value());

      printSensor("temperature_wakeup", temperatureWakeUp.value());
      printSensor("barometer_wakeup", barometerWakeUp.value());
      printSensor("humidity_wakeup", humidityWakeUp.value());
      printSensor("gas_wakeup", gasWakeUp.value());*/


    Serial.print("\n");
  }
}

void printSensor(String sensor_name, float sensor_value) {
  Serial.print(sensor_name + ":");
  Serial.print(sensor_value);
  Serial.print("\t");
}
