#include <Adafruit_NeoPixel.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>

#define PIN 6 // pin on the Arduino connected to the NeoPixels
#define M_WIDTH 8 // matrix width
#define M_HEIGHT 8 // matrix height

// The 8x8 matrix uses 800 KHz (v2) pixels that expect GRB color data.
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(8, 8, PIN, NEO_MATRIX_TOP + NEO_MATRIX_RIGHT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG, NEO_GRB + NEO_KHZ800 );


/**
   Test Graphics Primitives
   This sketch is based on Adafruit NeoPixel NeoMatrix 8x8 - 64 RGB LED Pixel Matrix: https://www.adafruit.com/product/1487
   Adafruit_NeoMatrix's drawing functions are taken from Adafruit_GFX library: https://learn.adafruit.com/adafruit-gfx-graphics-library/graphics-primitives
*/
void setup(void) {
  matrix.begin();
  matrix.setBrightness(20);

  matrix.fillScreen(0);
  matrix.show(); // send updated pixel colors to the hardware
}

void loop() {
  uint32_t c = matrix.Color(20, 200, 20);
  matrix.fillScreen(0);

  test_graphics_primitives(c);

  matrix.show();
  delay(500);
}

void test_graphics_primitives(uint32_t c) {
  unsigned short int custom_delay = 1500;
  
  matrix.fillScreen(0);
  rows_cols_test(c);
  matrix.show();
  delay(50);

  matrix.fillScreen(0);
  // void drawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color);
  matrix.drawLine(0, 2, 4, 7, c); // x0, y0, x1, y1, color
  matrix.show();
  delay(500);

  matrix.fillScreen(0);
  // void drawFastVLine(uint16_t x0, uint16_t y0, uint16_t length, uint16_t color);
  matrix.drawFastVLine(3, 2, 4, c); // x0, y0, length, color
  matrix.show();
  delay(500);

  matrix.fillScreen(0);
  // void drawFastHLine(uint8_t x0, uint8_t y0, uint8_t length, uint16_t color);
  matrix.drawFastHLine(0, 2, 4, c); // x0, y0, length, color
  matrix.show();
  delay(500);

  matrix.fillScreen(0);
  // void drawRect(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h, uint16_t color);
  matrix.drawRect(0, 2, 4, 3, c); // x0, y0, width, height, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void fillRect(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h, uint16_t color);
  matrix.fillRect(0, 1, 6, 4, c); // x0, y0, width, height, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void drawCircle(uint16_t x0, uint16_t y0, uint16_t r, uint16_t color);
  matrix.drawCircle(4, 4, 2, c); // x0, y0, radius, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void fillCircle(uint16_t x0, uint16_t y0, uint16_t r, uint16_t color);
  matrix.fillCircle(2, 3, 2, c); // x0, y0, radius, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void drawRoundRect(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h, uint16_t radius, uint16_t color);
  matrix.drawRoundRect(1, 1, 5, 4, 2, c); // x0, y0, width, height, radius, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void fillRoundRect(uint16_t x0, uint16_t y0, uint16_t r, uint16_t color);
  matrix.fillRoundRect(2, 2, 5, 4, 4, c); // x0, y0, width, height, radius, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void drawTriangle(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color);
  matrix.drawTriangle(0, 0, 3, 3, 0, 3, c); // x0, y0, x1, y1, x2, y2, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void fillTriangle(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color);
  matrix.fillTriangle(0, 4, 3, 4, 0, 7, c); // x0, y0, x1, y1, x2, y2, color
  matrix.show();
  delay(custom_delay);
}

void rows_cols_test(uint32_t c) {
  // void drawPixel(uint16_t x, uint16_t y, uint16_t color);
  int i, j;

  for (j = 0; j < M_HEIGHT; j++) {
    for (i = 0; i < M_WIDTH; i++) {
      matrix.fillScreen(0);
      matrix.drawPixel(i, j, c);
      delay(20);
      matrix.show();
    }
  }
}
