#include <Adafruit_NeoPixel.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>

#define PIN 6 // pin on the Arduino connected to the NeoPixels
#define M_WIDTH 8 // matrix width
#define M_HEIGHT 8 // matrix height

// The 8x8 matrix uses 800 KHz (v2) pixels that expect GRB color data.
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(8, 8, PIN, NEO_MATRIX_TOP + NEO_MATRIX_RIGHT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG, NEO_GRB + NEO_KHZ800 );

uint32_t red = matrix.Color(255, 0, 0);
uint32_t yellow = matrix.Color(255, 255, 0);
uint32_t green = matrix.Color(0, 255, 0);
uint32_t blue = matrix.Color(0, 0, 255);
uint32_t white = matrix.Color(255, 255, 255);

const int blink_time = 500;

/**
   Forbidden Access Signal Test
   This sketch is based on Adafruit NeoPixel NeoMatrix 8x8 - 64 RGB LED Pixel Matrix: https://www.adafruit.com/product/1487
   Adafruit_NeoMatrix's drawing functions are taken from Adafruit_GFX library: https://learn.adafruit.com/adafruit-gfx-graphics-library/graphics-primitives
*/
void setup(void) {
  matrix.begin();
  matrix.setBrightness(255);

  matrix.fillScreen(0);
  matrix.show(); // send updated pixel colors to the hardware
}

void loop() {
  forbidden_access(blue);
  delay(blink_time);
  forbidden_access(green);
  delay(blink_time);
  //  forbidden_access(yellow);
  //  delay(blink_time);
  //  forbidden_access(red);
  //  delay(blink_time);
}

void forbidden_access(uint32_t corner_color) {
  matrix.fillScreen(0);

  matrix.fillRect(0, 0, M_WIDTH, M_HEIGHT, red); // x0, y0, width, height, color
  matrix.fillRect(1, 3, 6, 2, white); // x0, y0, width, height, color
  matrix.drawTriangle(0, 0, 0, 1, 1, 0, corner_color); // x0, y0, x1, y1, x2, y2, color
  matrix.drawTriangle(7, 7, 6, 7, 7, 6, corner_color); // x0, y0, x1, y1, x2, y2, color
  matrix.drawTriangle(0, 7, 0, 6, 1, 7, corner_color); // x0, y0, x1, y1, x2, y2, color
  matrix.drawTriangle(7, 0, 6, 0, 7, 1, corner_color); // x0, y0, x1, y1, x2, y2, color

  matrix.show();
}
