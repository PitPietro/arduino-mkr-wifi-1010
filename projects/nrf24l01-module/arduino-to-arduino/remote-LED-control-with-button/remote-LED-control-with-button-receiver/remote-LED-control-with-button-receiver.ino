/*
   Documentation: https://nRF24.github.io/RF24

   Receiver - Receive the state of the button and light up a LED attached to digital pin 7
*/
#include <SPI.h>
#include "RF24.h"

// instantiate an object for the nRF24L01 transceiver
RF24 radio(9, 10); // using pin 9 for the CE pin, and pin 10 for the CSN pin

const byte address[6] = "00001";

// define the pin number to which the LED is connected
const short int led_pin = 7;

// initialize the button state to LOW
// this value will be read from radio communication and will light up the LED
bool button_state = 0;

void setup() {
  // initialize serial communication at 9600 baud rate
  Serial.begin(9600);
  // do not wait for serial port to connect.
  // let it works even if the user does not open the Serial Monitor

  // initialize the transceiver on the SPI bus
  if (!radio.begin()) {
    Serial.println("radio hardware is not responding!");
    while (1) {} // hold in infinite loop
  }

  Serial.print("This device is a receiver ~ RX");

  // Set the PA Level low to try preventing power supply related problems
  // because these examples are likely run with nodes in close proximity to
  // each other.
  radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default.
  
  // set the RX address of the TX node into a RX pipe
  radio.openReadingPipe(0, address);

  radio.startListening();

  // set the LED pin as digital output
  pinMode(led_pin, OUTPUT);
}

void loop() {
  radio.startListening();

  // while there is nothing available from the radio, do nothing
  while (!radio.available());

  // read the payload
  radio.read(&button_state, sizeof(button_state));

  // assign the button state to the LED
  digitalWrite(led_pin, button_state);

  Serial.print("Received button state: ");
  Serial.println(button_state);

  // slow transmissions down by 10 milliseconds
  delay(10);
}
