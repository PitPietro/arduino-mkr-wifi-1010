/*
   Joystick Position Sender

   Send via wireless the movements you can make with a joystick.

   Joystick wiring diagram:
   - GND --> GND
   -  5V --> VCC
   -  A2 --> VRX (analog, horizontal position) X-coordinate
   -  A3 --> VRY (analog, vertical position) Y-coordinate
   -  D7 --> SW (digital, push button) normally open

   nRF24l01 wiring diagram:
   - GND --> GND
   - D13 --> SCK
   - D12 --> MISO
   - D11 --> MOSI
   - D10 --> CSN
   - D09 --> CE
   -  /  --> IRQ
   - 3V3 --> VCC

*/
#include <SPI.h>
#include "RF24.h"
#include "printf.h"

#define MIN_THRESHOLD 400
#define MAX_THRESHOLD 800

const unsigned short int MIN_VALUE = 0;
const unsigned short int MIDDLE_VALUE = 510; // from 509 to 511
const unsigned short int MAX_VALUE = 1023;

char VRX_pin = A2;
char VRY_pin = A3;
char push_btn = 7;

unsigned short int x_value = 0;
unsigned short int y_value = 0;
boolean push_value = 0;

enum position {
  NONE,       // x =  510 ; y =  510
  UP,         // x =  510 ; y =    0
  DOWN,       // x =  510 ; y = 1023
  LEFT,       // x =    0 ; y =  510
  RIGHT,      // x = 1023 ; y =  510
  UP_RIGHT,   // x = 1023 ; y =    0
  UP_LEFT,    // x =    0 ; y =    0
  DOWN_RIGHT, // x = 1023 ; y = 1023
  DOWN_LEFT,  // x =    0 ; y = 1023
};

enum axis_approximation {
  MIDDLE, // MIN_THRESHOLD <= value <= MAX_THRESHOLD
  MIN,    // value < MIN_THRESHOLD
  MAX,    // MAX_THRESHOLD > value
};

axis_approximation x_axis, y_axis;
position joystick_position;

// instantiate an object for the nRF24L01 transceiver
// pin 09 for CE and pin 10 for CSN
RF24 radio(9, 10);

const byte address[6] = "00001";

void setup() {
  // initialize serial communication at 9600 baud rate
  Serial.begin(9600);

  // initialize the transceiver on the SPI bus
  if (!radio.begin()) {
    Serial.println("radio hardware is not responding!");
    while (1) {}  // hold in infinite loop
  }

  // Set the PA Level low to try preventing power supply related problems since
  // these examples are likely run with nodes in close proximity to each other.
  radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default.

  // set the TX address of the RX node into the TX pipe
  radio.openWritingPipe(address);

  // stop listening for incoming data
  radio.stopListening();

  // debugging info
  Serial.println("### start debugging info ###");
  printf_begin();  // needed only once for printing details
  // radio.printDetails();       // (smaller) function that prints raw register values
  radio.printPrettyDetails();  // (larger) function that prints human readable data
  Serial.println("### end debugging info ###\n");

  pinMode(push_btn, INPUT);
}

void loop() {
  radio.stopListening();
  x_value = analogRead(VRX_pin);
  y_value = analogRead(VRY_pin);
  push_value = digitalRead(push_btn);

  appoximate_position(x_value, &x_axis);
  appoximate_position(y_value, &y_axis);

  if (x_axis == axis_approximation::MIDDLE) {
    if (y_axis == x_axis) {
      joystick_position = position::NONE;
      Serial.print("NONE");
    } else if (y_axis == axis_approximation::MIN) {
      joystick_position = position::UP;
      Serial.print("UP");
    } else if (y_axis == axis_approximation::MAX) {
      joystick_position = position::DOWN;
      Serial.print("DOWN");
    }
  } else if (x_axis == axis_approximation::MIN) {
    if (y_axis == x_axis) {
      joystick_position = position::UP_LEFT;
      Serial.print("UP_LEFT");
    } else if (y_axis == axis_approximation::MIDDLE) {
      joystick_position = position::LEFT;
      Serial.print("LEFT");
    } else if (y_axis == axis_approximation::MAX) {
      joystick_position = position::DOWN_LEFT;
      Serial.print("DOWN_LEFT");
    }
  } else if (x_axis == axis_approximation::MAX) {
    if (y_axis == x_axis) {
      joystick_position = position::DOWN_RIGHT;
      Serial.print("DOWN_RIGHT");
    } else if (y_axis == axis_approximation::MIDDLE) {
      joystick_position = position::RIGHT;
      Serial.print("RIGHT");
    } else if (y_axis == axis_approximation::MIN) {
      joystick_position = position::UP_RIGHT;
      Serial.print("UP_RIGHT");
    }
  }

  // Serial.print("\t\t");
  // Serial.println(joystick_position);

  // start the timer
  unsigned long start_timer = micros();
  // transmit & save the report
  bool report = radio.write(&joystick_position, sizeof(position));
  // end the timer
  unsigned long end_timer = micros();

  if (report) {
    Serial.print("Transmission successful! "); // payload was delivered
    Serial.print("Time to transmit = ");
    Serial.print(end_timer - start_timer); // print the timer result
    Serial.print(" us. Sent: ");
    Serial.println(joystick_position); // print payload sent
  } else {
    // Serial.println("Transmission failed or timed out"); // payload was not delivered
  }

  delay(50);
}

void appoximate_position(int axis_value, axis_approximation* approx) {
  if (axis_value < MIN_THRESHOLD) {
    *approx = axis_approximation::MIN;
  } else if (axis_value > MAX_THRESHOLD) {
    *approx = axis_approximation::MAX;
  } else if (MIN_THRESHOLD <= axis_value && axis_value <= MAX_THRESHOLD) {
    *approx = axis_approximation::MIDDLE;
  }
}
