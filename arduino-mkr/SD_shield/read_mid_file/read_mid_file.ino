/*
   READ .mid file

   Here's a basic overview of how you can interpret a MIDI file:
   - Header Chunk: The MIDI file starts with a header chunk that specifies the file format, number of tracks, and timing information.
   - Track Chunks: After the header chunk, there are one or more track chunks. Each track chunk represents a sequence of MIDI events.
   - Event Parsing: Within each track chunk, MIDI events are encoded. Common MIDI events include note on, note off, tempo change, control change, etc.
   - Timing: MIDI events are timestamped with delta times, which represent the number of ticks since the previous event.
   - Data Extraction: Extract relevant data from MIDI events, such as note number, velocity, channel, etc.

   Waiting for deltaTime is crucial for maintaining the correct timing when playing back MIDI events. The deltaTime represents the time interval between the current MIDI event and the previous one, measured in ticks. By waiting for this interval, you ensure that MIDI events are played back at the correct tempo and rhythm as specified in the MIDI file.
   If you don't wait for deltaTime, MIDI events will be processed as fast as the Arduino can handle, potentially leading to incorrect timing and playback. Without the proper timing, the music will not sound as expected, and notes may be played too quickly or with incorrect durations.

   In the provided example code, the delay(deltaTime) statement ensures that the Arduino waits for the appropriate amount of time before processing the next MIDI event. This delay allows you to synchronize the playback of MIDI events with the timing specified in the MIDI file.

   If you're implementing a MIDI player or sequencer, accurate timing is essential for producing high-quality audio output. Therefore, it's crucial to wait for deltaTime to ensure proper synchronization and timing accuracy in MIDI playback.

   SD.h https://www.arduino.cc/en/Reference/SD
   SD.h notes https://www.arduino.cc/en/Reference/SDCardNotes
*/
#include <SD.h>
File midiFile;

/*
TODOs
1. check why there are only note off events (code error?)
*/


// default SPI's CS pin for Arduino MKR SD Proto Shield
const short int chipSelect = 4;

String filepath = "TEST.MID";

void readFromSD() {
  File dataFile = SD.open(filepath, FILE_READ);

  Serial.println("\nReading from SD");

  while (dataFile.available()) {
    char c = dataFile.read();
    Serial.write(c);
  }

  dataFile.close();

  Serial.println("\nDone reading from SD");
}

void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  Serial.println("Initializing SD card");

  // check if the card is present and can be initialized
  if (!SD.begin(chipSelect)) {
    Serial.println("SD Card failed, or not present");
    // don't do anything more
    while (true);
  }
  Serial.println("Done: SD Card initialized");

  midiFile = SD.open(filepath);
  if (!midiFile) {
    Serial.println("Failed to open MIDI file!");
    return;
  }

  // Skip header chunk
  midiFile.seek(14);
}

void loop() {
  // Read MIDI event
  int deltaTime = readVariableLengthValue();
  byte statusByte = midiFile.read();

  if (statusByte == 0xFF) {
    // Meta event, handle as needed
    byte metaType = midiFile.read();
    int length = readVariableLengthValue();

    Serial.print("metaType: ");
    Serial.print(metaType);
    Serial.print(" \t ~ \t length: ");
    Serial.println(length);

    // Handle meta event
  } else if (statusByte >= 0x80 && statusByte <= 0xEF) {
    // MIDI event, extract channel and type
    int channel = (statusByte & 0x0F) + 1;
    int eventType = (statusByte & 0xF0) >> 4;
    // Handle MIDI event
    switch (eventType) {
      case 0x08: {
          // Note Off
          byte noteNumber = midiFile.read();
          byte velocity = midiFile.read();
          // Handle note off event

          Serial.print("~~~ note off event: ");
          Serial.print("noteNumber: ");
          Serial.print(noteNumber);
          Serial.print(" ~\t velocity: ");
          Serial.println(velocity);
          break;
        }

      case 0x09: {
          // Note On
          byte noteNumber = midiFile.read();
          byte velocity = midiFile.read();
          // Handle note on event

          Serial.print("~~~ note on event: ");
          Serial.print("noteNumber: ");
          Serial.print(noteNumber);
          Serial.print(" ~\t velocity: ");
          Serial.println(velocity);
          break;
        }
      default: {
          // Handle other MIDI event types
          Serial.print("default handler - eventType: ");
          Serial.println(eventType);
          break;
        }
    }
  }
  // Wait for next event using deltaTime
  // Serial.print("Wait for ");
  // Serial.println(deltaTime);
  delay(deltaTime);
}

int readVariableLengthValue() {
  int value = 0;
  byte buffer;
  do {
    buffer = midiFile.read();
    value = (value << 7) + (buffer & 0x7F);
  } while (buffer & 0x80);
  return value;
}
