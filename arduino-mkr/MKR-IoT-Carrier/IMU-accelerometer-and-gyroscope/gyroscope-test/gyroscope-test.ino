#include <Arduino_MKRIoTCarrier.h>

MKRIoTCarrier carrier;
float x, y, z;

/**
 * Gyroscope Test
 * 
 * <Arduino_MKRIoTCarrier.h> includes <Arduino_LSM6DS3.h> library to control LSM6DSOXTR microcontroller.
 * Docs: https://www.arduino.cc/reference/en/libraries/arduino_lsm6ds3/
 */
void setup() {
  // initialize serial communication at 9600 baud rate
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only.
  while (!Serial);

  // initialize the MKR IoT Carrier and output any errors in the serial monitor
  CARRIER_CASE = false;
  carrier.begin();
}

void loop() {
  // query if new gyroscope data from the IMU is available
  // returns 0 if no new gyroscope data is available, 1 if new gyroscope data is available
  if (carrier.IMUmodule.gyroscopeAvailable()) {
    // query the IMU's gyroscope, return angular speed in dps (degrees per second)
    carrier.IMUmodule.readGyroscope(x, y, z);
    
    // print the gyroscope information on the serial monitor
    print_gyroscope();
  }

  // always place a short delay before closing the loop
  delay(100);
}

void print_gyroscope() {
  Serial.print("x: ");
  Serial.print(x);
  Serial.print(", y: ");
  Serial.print(y);
  Serial.print(", z: ");
  Serial.println(z);
}
